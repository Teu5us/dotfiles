# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, options, ... }:

with builtins;

let
  unstable = import <unstable> { };
  kmonad = import ./kmonad/kmonad.nix;
in {
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ./cachix.nix
  ];

  fileSystems."/".options = [ "noatime" "nodiratime" "discard" ];
  fileSystems."/home".options = [ "noatime" "nodiratime" "discard" ];

  # Use the systemd-boot EFI boot loader.
  boot = {
    initrd = {
      luks.devices = {
        root = {
          # name = "root";
          # device = "/dev/sdb2";
          device = "/dev/disk/by-uuid/9d967617-5ad2-45c4-9dc8-b4c6ca3191a6";
          preLVM = true;
          allowDiscards = true;
        };
        home = {
          # name = "home";
          # device = "/dev/sda1";
          device = "/dev/disk/by-uuid/8953056d-b75f-4907-ae30-558cbc853657";
          preLVM = true;
          allowDiscards = true;
        };
      };
    };

    kernel.sysctl = {
      # change ttl
      "net.ipv4.ip_default_ttl" = 65;
      # NETWORK
      "net.core.somaxconn" = 256;
      "net.core.rmem_default" = 1048576;
      "net.core.rmem_max" = 16777216;
      "net.core.wmem_default" = 1048576;
      "net.core.wmem_max" = 16777216;
      "net.core.optmem_max" = 65536;
      "net.ipv4.tcp_rmem" = "4096 1048576 2097152";
      "net.ipv4.tcp_wmem" = "4096 65536 16777216";
      # "net.ipv4.tcp_mem" = "524288 524288 524288";
      "net.ipv4.udp_rmem_min" = 8192;
      "net.ipv4.udp_wmem_min" = 8192;
      "net.ipv4.tcp_slow_start_after_idle" = 0;
      "net.ipv4.tcp_fastopen" = 3;
      # "net.core.default_qdisc" = "fq";
      # "net.ipv4.tcp_congestion_control" = "bbr";
      "net.ipv4.tcp_congestion_control" = "cubic";
      "net.ipv4.tcp_rfc1337" = 1;
      "net.ipv4.ip_no_pmtu_disc" = 0;
      "net.ipv4.tcp_mtu_probing" = 1;
      "net.ipv4.tcp_sack" = 1;
      "net.ipv4.tcp_fack" = 1;
      "net.ipv4.tcp_window_scaling" = 1;
      "net.ipv4.tcp_workaround_signed_windows" = 1;
      "net.ipv4.tcp_timestamps" = 1;
      "net.ipv4.tcp_ecn" = 0;
      "net.ipv4.route.flush" = 1;
      "net.ipv4.tcp_low_latency" = 1;
      "net.ipv4.tcp_frto" = 2;

      # VM
      "vm.swappiness" = 3;
      "vm.vfs_cache_pressure" = 50;
      "vm.watermark_scale_factor" = 200;
      "vm.dirty_ratio" = 3;
      "vm.dirty_writeback_centisecs" = 1500;
      "vm.laptop_mode" = 5;
    };

    loader = {
      systemd-boot = {
        enable = true;
        consoleMode = "max";
        editor = false;
      };
      efi.canTouchEfiVariables = true;
    };

    # kernelPackages = pkgs.linuxPackages_latest;
    kernelParams = [ "quiet" "loglevel=0" "consoleblank=0" "pcie_aspm=force" "acpi_osi=" "usbcore.autosuspend=1" ];
    supportedFilesystems = [ "ntfs" "exfat" ];
    extraModprobeConfig = ''
      options i915 enable_fbc=1 enable_dc=2 enable_psr=1 fastboot=0 enable_dpcd_backlight=1
      options drm vblankoffdelay=1
      options snd_hda_intel power_save=1
      options iwlwifi power_save=1 power_level=5
      options thinkpad_acpi fan_control=1
      options kvm ignore_msrs=1
    '';
    kernelModules = [ "acpi_call" "intel_agp" "i915" "uinput" ];
    extraModulePackages = with config.boot.kernelPackages; [
      acpi_call
      anbox
    ];
    cleanTmpDir = true;
  };

  security = {
    sudo.enable = true;
    wrappers.spice-client-glib-usb-acl-helper = {
      source = "${pkgs.spice-gtk}/bin/spice-client-glib-usb-acl-helper";
      capabilities = "cap_fowner+ep";
    };
  };

  networking = {
    hostName = "nix450s"; # Define your hostname.
    wireless = {
      enable = true; # Enables wireless support via wpa_supplicant.
      interfaces = [ "wlp3s0" ];
      networks = {
      };
    };
    networkmanager = {
      enable = true; # Enable networkmanager
      unmanaged = [ "wlp3s0" ];
      packages = [ pkgs.networkmanager-openvpn ];
    };

    # The global useDHCP flag is deprecated, therefore explicitly set to false here.
    # Per-interface useDHCP will be mandatory in the future, so this generated config
    # replicates the default behaviour.
    useDHCP = false;
    interfaces.enp0s25.useDHCP = false;
    interfaces.wlp3s0.useDHCP = true;

    # Configure network proxy if necessary
    # proxy.default = "localhost:8118";
    # proxy.noProxy = "127.0.0.1,localhost,internal.domain";
    # Open ports in the firewall.
    firewall.allowedTCPPorts = [ 22 3020 3350 3389 5432 ];
    # firewall.allowedUDPPorts = [ ... ];
    # Or disable the firewall altogether.
    firewall.enable = true;
  };

  console = {
    earlySetup = true;
    font = "Lat2-Terminus16";
    keyMap = "emacs";
    colors = [
      ## gruvbox
      # "282828"
      # "cc241d"
      # "98971a"
      # "d79921"
      # "458588"
      # "b16286"
      # "689d6a"
      # "a89984"
      # "928374"
      # "fb4934"
      # "b8bb26"
      # "fabd2f"
      # "83a598"
      # "d3869b"
      # "8ec07c"
      # "ebdbb2"

      ## nord
      "3B4252"
      "BF616A"
      "A3BE8C"
      "EBCB8B"
      "81A1C1"
      "B48EAD"
      "88C0D0"
      "E5E9F0"
      "4C566A"
      "BF616A"
      "A3BE8C"
      "EBCB8B"
      "81A1C1"
      "B48EAD"
      "8FBCBB"
      "ECEFF4"
    ];
  };

  # Select internationalisation properties.
  i18n = {
    defaultLocale = "en_US.UTF-8";
    supportedLocales =
      [ "en_US.UTF-8/UTF-8" "ru_RU.UTF-8/UTF-8" "de_DE.UTF-8/UTF-8" ];
  };

  # Set your time zone.
  time.timeZone = "Asia/Omsk";

  nix.trustedUsers = [ "root" "paul" ];
  nix.nixPath =
    options.nix.nixPath.default ++
    [ "nixpkgs-overlays=/etc/nixos/overlays-compat/" ];

  nixpkgs = {
    overlays = [
      (self: super: {
        zu-django-dev = super.callPackage ./zu-django-dev/default.nix { };
      })
    ];
    config = {
      allowUnfree = true;
      permittedInsecurePackages = [
        "p7zip-16.02"
      ];
    };
  };

  environment.loginShellInit = ''
GUIX_PROFILE="$HOME/.guix-profile"
source "$GUIX_PROFILE/etc/profile"
source /var/guix/profiles/per-user/root/current-guix/etc/profile
export GUIX_LOCPATH=$HOME/.guix-profile/lib/locale
# export PATH="$HOME/.guix-profile/bin:$HOME/.guix-profile/sbin:$PATH" # set by sourcing guix profile
export XDG_DATA_DIRS="$HOME/.guix-profile/share:$XDG_DATA_DIRS"
# export GUILE_LOAD_PATH="/home/paul/.guix-profile/share/guile/site/2.2"
# export GUILE_LOAD_COMPILED_PATH="/home/paul/.guix-profile/lib/guile/2.2/site-ccache"
export GUILE_LOAD_PATH="$HOME/.guix-profile/share/guile/site/3.0:/var/guix/profiles/per-user/root/current-guix/lib/guile/3.0/site-ccache"
export GUILE_LOAD_COMPILED_PATH="$HOME/.guix-profile/lib/guile/3.0/site-ccache:$HOME/.guix-profile/share/guile/site/3.0"
# export EMACSLOADPATH="$HOME/.guix-profile/share/emacs/site-lisp:$HOME/.guix-profile/share/emacs/28.0.50/lisp"
export INFOPATH="$HOME/.guix-profile/share/info:/var/guix/profiles/per-user/root/current-guix/share/info:$INFOPATH"
export MANPATH="$HOME/.guix-profile/share/man:/var/guix/profiles/per-user/root/current-guix/share/man:$MANPATH"
export QT_QPA_PLATFORMTHEME=gtk3
FONTCONFIG_FILE=/etc/fonts/fonts.conf
'';

  environment.extraSetup = ''
ln -s /var/guix/profiles/per-user/root/current-guix/bin/guix $out/bin/guix
ln -s /var/guix/profiles/per-user/root/current-guix/share/zsh/site-functions/_guix $out/share/zsh/site-functions/_guix
'';

  environment.pathsToLink = [
    "/share/zsh"
  ];

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    kmonad
    ccache
    findutils
    unstable.vim
    # unstable.neovim
    unstable.neovim-remote
    tor
    tdesktop
    pciutils
    wget
    aspell
    curl
    gitAndTools.gitFull
    nix-prefetch-scripts
    which
    htop
    unstable.bash
    unstable.bash-completion
    unstable.zsh
    unstable.zsh-completions
    w3m
    ranger
    xcape
    xorg.xmodmap
    xorg.xkbcomp
    trayer
    i3lock
    psmisc
    nodejs
    compton
    global
    neofetch
    powertop
    thermald
    tlp
    lm_sensors
    # texlive.combined.scheme-full
    wineWowPackages.full
    (winetricks.override { wine = wineWowPackages.full; })
    cups-brother-hl1110
    (chromium.override { enableVaapi = true; })
    qemu
    virtmanager
    spice-gtk
    unstable.virglrenderer
  ];

  fonts = {
    fonts = with pkgs; [
      opensans-ttf
      dejavu_fonts
      corefonts
      powerline-fonts
      iosevka
      paratype-pt-serif
      paratype-pt-sans
      unifont
      emacs-all-the-icons-fonts
      joypixels
      roboto-mono
      google-fonts
      fira-code
    ];
    enableFontDir = true;
    fontconfig = {
      enable = true;
      hinting = {
        autohint = false;
        enable = true;
      };
      subpixel.lcdfilter = "default";
      antialias = true;
      penultimate.enable = false;
      includeUserConf = true;
      defaultFonts = {
        serif = [ "Tinos" ];
        sansSerif = [ "Arimo" ];
        monospace = [ "Iosevka" ];
      };
    };
  };

  virtualisation = {
    # libvirtd
    libvirtd = {
      enable = true;
      qemuVerbatimConfig =
        ''
          user = "paul"
          group = "libvirtd"
        '';
    };
    # Docker
    docker.enable = true;
    # Anbox
    anbox.enable = true;
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs = {
    ccache = {
      enable = true;
      packageNames = [
        # "python37Packages.pytorch"
        # "python37Packages.pytorchWithCuda"
        # "magma"
        # "openmpi"
      ];
    };
    adb.enable = true;
    mtr.enable = true;
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
    chromium = {
      enable = true;
      extensions = [
        "bgnkhhnnamicmpeenaelnjfhikgbkllg" # adguard
        "dbepggeogbaibhgnhhndojpepiihcmeb" # vimium
        "eimadpbcbfnmbkopoojfekhnkhdbieeh" # darkreader
        "hlepfoohegkhhmjieoechaddaejaokhf" # refined github
        "chphlpgkkbolifaimnlloiipkdnihall" # onetab
        "lhaoghhllmiaaagaffababmkdllgfcmc" # atomic-chrome
        "egpjdkipkomnmjhjmdamaniclmdlobbo" # firenvim
        "elifhakcjgalahccnjkneoccemfahfoa" # markdown here
      ];
    };
    ssh.forwardX11 = true;
  };

  powerManagement = {
    enable = true;
    # scsiLinkPolicy = "min_power";
    powertop.enable = false;
  };

  systemd.services.guix-daemon = {
    enable = true;
    description = "Build daemon for GNU Guix";
    serviceConfig = {
      ExecStart = "/var/guix/profiles/per-user/root/current-guix/bin/guix-daemon --build-users-group=guixbuild";
      Environment="GUIX_LOCPATH=/root/.guix-profile/lib/locale";
      RemainAfterExit="yes";
      StandardOutput="syslog";
      StandardError="syslog";
      TaskMax= "8192";
    };
    wantedBy = [ "multi-user.target" ];
  };

  services = {
    nixosManual.showManual = false;
    kmscon = {
      enable = false;
      hwRender = true;
      extraConfig = ''
        xkb-layout=us
        font-name=Iosevka
        font-size=18
      '';
    };
    nscd.enable = true;
    thinkfan = {
      enable = true;
      sensors =
        ''
          tp_thermal /proc/acpi/ibm/thermal
        '';
    };
    logind = {
      lidSwitch = "suspend";
      lidSwitchDocked = "ignore";
      lidSwitchExternalPower = "ignore";
    };
    thermald.enable = true;
    tlp = {
      enable = true;
      extraConfig = builtins.readFile ./tlp.conf;
    };
    # Enable the OpenSSH daemon.
    openssh = {
      enable = true;
      permitRootLogin = "no";
      forwardX11 = false;
    };

    # Enable CUPS to print documents.
    printing.enable = false;

    # Enable tor
    tor = {
      enable = true;
      client.enable = true;
    };

    udev = {
      extraRules =
        ''
          SUBSYSTEM=="usb", MODE="0666"
          # KMonad user access to /dev/uinput
          KERNEL=="uinput", MODE="0660", GROUP="uinput", OPTIONS+="static_node=uinput"
        '';
    };

    # Configure Xorg
    xserver = {
      enable = true;
      autorun = true;
      layout = "us,ru";
      xkbVariant = ",";
      xkbOptions = "grp:shifts_toggle";
      # xkbOptions = "lv3:ralt_switch, grp_led:caps, caps:super";
#       extraLayouts = {
#         colemak-dh = {
#           description = "Light colemak-dh variant.";
#           languages = [ "eng" ];
#           compatFile = builtins.toFile "colemak-dh.xkb" ''
# xkb_compatibility "colemak-dh" {
#    interpret ISO_Next_Group { action = LockGroup(group=+1); };
#    interpret ISO_Prev_Group { action = LockGroup(group=-1); };
#    interpret ISO_Group_Latch { action = LatchGroup(group=2); };
# };
#             '';
#           symbolsFile = builtins.toFile "colemak-dh.xkb" ''
# xkb_symbols "colemak-dh" {
#
#     include "us"
#     name[Group1]= "English (Colemak)";
#
#     key <TLDE> { [        grave,   asciitilde ] };
#     key <AE01> { [            1,       exclam ] };
#     key <AE02> { [            2,           at ] };
#     key <AE03> { [            3,   numbersign ] };
#     key <AE04> { [            4,       dollar ] };
#     key <AE05> { [            5,      percent ] };
#     key <AE06> { [            6,  asciicircum ] };
#     key <AE07> { [            7,    ampersand ] };
#     key <AE08> { [            8,     asterisk ] };
#     key <AE09> { [            9,    parenleft ] };
#     key <AE10> { [            0,   parenright ] };
#     key <AE11> { [        minus,   underscore ] };
#     key <AE12> { [        equal,         plus ] };
#
#     key <AD01> { [            q,            Q ] };
#     key <AD02> { [            w,            W ] };
#     key <AD03> { [            f,            F ] };
#     key <AD04> { [            p,            P ] };
#     key <AD05> { [            b,            B ] };
#     key <AD06> { [            j,            J ] };
#     key <AD07> { [            l,            L ] };
#     key <AD08> { [            u,            U ] };
#     key <AD09> { [            y,            Y ] };
#     key <AD10> { [    semicolon,        colon ] };
#     key <AD11> { [  bracketleft,    braceleft ] };
#     key <AD12> { [ bracketright,   braceright ] };
#     key <BKSL> { [    backslash,          bar ] };
#
#     key <AC01> { [            a,            A ] };
#     key <AC02> { [            r,            R ] };
#     key <AC03> { [            s,            S ] };
#     key <AC04> { [            t,            T ] };
#     key <AC05> { [            g,            G ] };
#     key <AC06> { [            k,            K ] };
#     key <AC07> { [            n,            N ] };
#     key <AC08> { [            e,            E ] };
#     key <AC09> { [            i,            I ] };
#     key <AC10> { [            o,            O ] };
#     key <AC11> { [   apostrophe,     quotedbl ] };
#
#     key <AB01> { [            x,            X ] };
#     key <AB02> { [            c,            C ] };
#     key <AB03> { [            d,            D ] };
#     key <AB04> { [            v,            V ] };
#     key <AB05> { [            z,            Z ] };
#     key <AB06> { [            m,            M ] };
#     key <AB07> { [            h,            H ], [ BackSpace ] };
#     key <AB08> { [        comma,         less ] };
#     key <AB09> { [       period,      greater ] };
#     key <AB10> { [        slash,     question ] };
#
#     key <CAPS> { [    BackSpace,    BackSpace ] };
#     key <LSGT> { [        minus,   underscore ] };
#     key <SPCE> { [        space,        space ] };
#
#     key <PRSC> { [ ISO_Next_Group ], [ ISO_Prev_Group ] };
# };
#           '';
#         };
#       };
      libinput = {
        enable = true;
        tapping = false;
        naturalScrolling = true;
        disableWhileTyping = true;
        # accelSpeed = "-0.5";
        # dev = "/dev/input/event16";
      };
      inputClassSections = [
        ''
          Identifier "Logitech MX Ergo"
          MatchProduct "MX Ergo Mouse"
          MatchIsPointer "on"
          MatchDevicePath "/dev/input/event*"
          Driver "evdev"
          Option "SendCoreEvents" "true"
          Option "EmulateWheel" "true"
          Option "EmulateWheelButton" "9"
          Option "XAxisMapping" "6 7"
          Option "YAxisMapping" "4 5"
          Option "DeviceAccelProfile" "2"
          Option "AdaptiveDeceleration" "2"
          Option "VelocityScale" "1.7"
        ''
      ];
      displayManager = {
        # Enable startx
        startx.enable = true;
      };
      # xrandrHeads = [ "LVDS-1" "VGA-1" ];
      videoDrivers = [ "modesetting" ];
      useGlamor = true;
    };
    postgresql = {
      enable = true;
      package = pkgs.postgresql;
      enableTCPIP = true;
      authentication = pkgs.lib.mkOverride 10 ''
        local all all trust
        host all all ::1/128 trust
      '';
      initialScript = pkgs.writeText "backend-initScript" ''
        CREATE ROLE admin WITH LOGIN PASSWORD 'admin' CREATEDB;
        CREATE DATABASE admin;
        GRANT ALL PRIVILEGES ON DATABASE admin TO admin;
      '';
    };
  };

  xdg.portal.gtkUsePortal = true;

  # Enable sound.
  sound.enable = true;

  hardware = {
    bluetooth = {
      enable = true;
      package = pkgs.bluezFull;
    };
    pulseaudio = {
      enable = true;
      package = pkgs.pulseaudioFull;
      extraModules = [ pkgs.pulseaudio-modules-bt ];
      extraConfig = ''
load-module module-switch-on-connect
      '';
    };
    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
      extraPackages = with pkgs; [
        vaapiIntel
        libvdpau-va-gl
        vaapiVdpau
        vulkan-loader
        intel-media-driver
      ];
      extraPackages32 = with pkgs.pkgsi686Linux; [
        vaapiIntel
        libvdpau-va-gl
        vaapiVdpau
        vulkan-loader
      ];
    };
    cpu.intel.updateMicrocode = true;
    usbWwan.enable = true;
    trackpoint = {
      enable = true;
      emulateWheel = true;
      sensitivity = 250;
      speed = 200;
    };
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    mutableUsers = false;
    extraUsers =
      let
        paulUser = {
          paul = {
            isNormalUser = true;
            createHome = true;
            extraGroups = [
              "wheel"
              "video"
              "audio"
              "disk"
              "networkmanager"
              "input"
              "uinput"
              "docker"
              "kvm"
              "vboxusers"
              "libvirtd"
              "tty"
            ]; # Enable ‘sudo’ and other groups for the user.
            group = "users";
            home = "/home/paul";
            uid = 1000;
            # also set password here 
            shell = unstable.zsh;
          };
        };
        buildUser = (i:
          {
            "guixbuilder${i}" = {
              group = "guixbuild";
              extraGroups = ["guixbuild"];
              home = "/var/empty";
              shell = pkgs.nologin;
              description = "Guix build user ${i}";
              isSystemUser = true;
            };
          }
        );
      in
        # merge all users
        pkgs.lib.fold (str: acc: acc // buildUser str)
          paulUser
          (map (pkgs.lib.fixedWidthNumber 2) (builtins.genList (n: n+1) 10));
    extraGroups.guixbuild = {
      name = "guixbuild";
    };
    extraGroups.uinput = {
      name = "uinput";
    };
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "20.03"; # Did you read the comment?

  # system.nixos.tags = [ "latestKernel" ];

}
