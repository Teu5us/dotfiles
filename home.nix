{ config, pkgs, ... }:

with import <nixpkgs> { };
with builtins;

# shortcuts
let
  unstable = import <unstable> { };
  aliases = {
    # v = "nvim";
    nec = "ecl /etc/nixos/configuration.nix";
    nvc = "sudo -E nvim /etc/nixos/configuration.nix";
    eec = "ecl ~/.config/nixpkgs/derive/emacs/default.nix";
    evc = "nvim ~/.config/nixpkgs/derive/emacs/default.nix";
    hec = "ecl ~/.config/nixpkgs/home.nix";
    hvc = "nvim ~/.config/nixpkgs/home.nix";
    hep = "ecl ~/.config/nixpkgs/config.nix";
    hvp = "nvim ~/.config/nixpkgs/config.nix";
    hm = "home-manager";
    cp = "cp -i";
    mkd = "mkdir -pv";
    # ls = "ls -hN --color=auto --group-directories-first";
    ls = "exa --group-directories-first";
    ll = "exa --group-directories-first -l";
    la = "exa --group-directories-first -a";
    lla = "exa --group-directories-first -al";
    r = "$FILE";
    sr = "sudo -E $FILE";
    doom = "~/.emacs.d/bin/doom";
    doom-rebuild = "doom clean && doom sync && doom compile";
    calcurse = "calcurse -D ~/.config/calcurse";
    e = "$EDITOR";
    x = "sxiv -ft *";
    grep = "grep --color=auto";
    diff = "diff --color=auto";
    yt = "youtube-dl --add-metadata -i -o '%(upload_date)s-$(title)-.%(ext)s'";
    yta = "youtube-dl -x -f bestaudio/best";
    YT = "youtube-viewer";
    vm = "VBoxManage startvm";
    hasearch = "nix-env -f '<nixpkgs>' -qaP -A haskellPackages | grep";
    srcp =
      "rsync -avz -e 'ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null' --progress";
    run = "setsid --fork";
  };
  sessionVars = {
    PATH = ''
      $(du "$HOME/.local/bin/" | cut -f2 | tr "\n" ":" | sed "s/:*$//"):$PATH
    '';
    VISUAL = "ecl";
    EDITOR = "ecl";
    TERMINAL = "alacritty";
    BROWSER = "chromium";
    READER = "zathura";
    FILE = "ranger";
    SUDO_ASKPASS = "";
    NVIM_LISTEN_ADDRESS = "/tmp/nvimsocket";
    DIGESTIFDATA =
      "/nix/store/fay8gx9w5rkx8j1qmjwdvx8w4kzmhbbn-lua5.3-digestif-0.2-1/digestif-0.2-1-rocks/digestif/0.2-1/data";
    CHICKEN_REPOSITORY_PATH = "$HOME/.nix-profile/lib/chicken/11";
    CHICKEN_INCLUDE_PATH = "$HOME/.nix-profile/share";
  };
  nord0 = "#2E3440";
  nord1 = "#3B4252";
  nord2 = "#434C5E";
  nord3 = "#4C566A";
  nord4 = "#D8DEE9";
  nord5 = "#E5E9F0";
  nord6 = "#ECEFF4";
  nord7 = "#8FBCBB";
  nord8 = "#88C0D0";
  nord9 = "#81A1C1";
  nord10 = "#5E81AC";
  nord11 = "#BF616A";
  nord12 = "#D08770";
  nord13 = "#EBCB8B";
  nord14 = "#A3BE8C";
  nord15 = "#B48EAD";
  serifFont = "Tinos";
  sansFont = "Arimo";
  monoFont = "Fira Code";
  kbdLayout = "us,ru";
  kbdVariant = "dvorak,";
  kbdOptions = [ "lv3:ralt_switch" "grp_led:caps" "caps:super" ];
  spare_modifier = "Hyper_L";
  nix220calib = "~/.config/Lenovo_Thinkpad_X220_IPS_1366x768.icm";
  nix450scalib = "~/.config/nixpkgs/home/config/t450s_mac.icc";
  # xinit = ''
  #   # eval `dbus-launch --auto-syntax`

  #   xrdb ~/.Xresources &
  #   xhost +SI:localuser:$USER

  #   export XMODIFIERS=@im=exwm-xim
  #   export GTK_IM_MODULE=xim
  #   export QT_IM_MODULE=xim
  #   export CLUTTER_IM_MODULE=xim

  #   [ ! -d ~/shells ] && ln -s ~/.config/nixpkgs/home/shells ~/

  #   xset r rate 300 60

  #   setxkbmap \
  #     -layout ${kbdLayout} \
  #     -variant ${kbdVariant} \
  #     -option lv3:ralt_switch \
  #     -option grp_led:caps \
  #     -option caps:super \
  #     -option grp:shifts_toggle \
  #     -option ctrl:swap_lalt_lctl \
  #     -option ctrl:rctrl_ralt \
  #     -option ctrl:ralt_rctrl

  #   # killall xcape
  #   # xcape -e "Super_L=Escape;Super_R=Escape"

  #   feh --bg-scale ~/.config/walls/image_2020-04-30_16-34-21.png &
  #   clipmenud &
  #   # dwmbar &
  #   xcalib ${nix450scalib} &
  #   xinput set-prop "TPPS/2 IBM TrackPoint" "libinput Accel Speed" "-0.6" &
  #   # xinput disable 'Synaptics TM3053-004'
  #   # emacs --daemon &

  #   xmodmap -e "clear Mod3"
  #   xmodmap -e "add Mod3 = Hyper_L"

  #   # unused now:

  #   # # menu as right super
  #   # xmodmap -e "keycode 135 = Super_R"

  #   # setxkbmap -layout ${kbdLayout} -variant ${kbdVariant} -option lv3:ralt_switch -option grp_led:caps -option caps:super -option grp:shifts_toggle

  #   # Map SPC as additional C
  #   # xmodmap -e "keycode 23 = ${spare_modifier}"
  #   xmodmap -e "keycode 65 = ${spare_modifier}"
  #   xmodmap -e "remove mod4 = ${spare_modifier}"
  #   # hyper_l is mod4 by default
  #   # xmodmap -e "add Control = ${spare_modifier}"
  #   xmodmap -e "keycode any = space"
  #   killall xcape
  #   xcape -e "${spare_modifier}=space;Super_L=Escape;Super_R=Escape"
  # '';
  xinit = ''
    # eval `dbus-launch --auto-syntax`

    xrdb ~/.Xresources &
    xhost +SI:localuser:$USER

    export XMODIFIERS=@im=exwm-xim
    export GTK_IM_MODULE=xim
    export QT_IM_MODULE=xim
    export CLUTTER_IM_MODULE=xim

    [ ! -d ~/shells ] && ln -s ~/.config/nixpkgs/home/shells ~/

    xset r rate 300 60

    feh --bg-scale ~/.config/walls/image_2020-04-30_16-34-21.png &
    clipmenud &
    xcalib ${nix450scalib} &
    xinput set-prop "TPPS/2 IBM TrackPoint" "libinput Accel Speed" "-0.6" &
    xinput disable 'Synaptics TM3053-004'
    kmonad ~/.config/kmonad-keymap.kbd &
  '';
  exwmCommand = "dbus-launch --sh-syntax --exit-with-session emacs --eval '(run-exwm)'";
in rec {
  nixpkgs.config.allowUnfree = true;
  # Programs
  programs = {
    # Let Home Manager install and manage itself.
    home-manager.enable = true;
    # Add and configure git
    emacs = {
      enable = true;
      package = unstable.emacsGccWrapped;
      extraPackages = (epkgs: with epkgs; [
        pdf-tools
        elpy
        haskell-mode
        hamlet-mode
        lispy
        evil-lispy
        exwm
        jupyter
      ]);
    };
    git = {
      enable = true;
      userName = "Pavel Stepanov";
      userEmail = "pavelstepanov@vivaldi.net";
    };
    alacritty = {
      enable = true;
      settings = {
        window = {
          dimensions = {
            lines = 40;
            cols = 80;
          };
          padding = {
            x = 1;
            y = 1;
          };
          dynamic_padding = true;
          decorations = "none";
        };
        scrolling = {
          history = 10000;
          multiplier = 5;
        };
        font = { size = 9; };
        colors = {
          primary = {
            background = "${nord0}";
            foreground = "${nord4}";
          };
          cursor = {
            text = "${nord0}";
            cursor = "${nord4}";
          };
          normal = {
            black = "${nord1}";
            red = "${nord11}";
            green = "${nord14}";
            yellow = "${nord13}";
            blue = "${nord9}";
            magenta = "${nord15}";
            cyan = "${nord8}";
            white = "${nord5}";
          };
          bright = {
            black = "${nord3}";
            red = "${nord11}";
            green = "${nord14}";
            yellow = "${nord13}";
            blue = "${nord9}";
            magenta = "${nord15}";
            cyan = "${nord7}";
            white = "${nord6}";
          };
        };
        background_opacity = 0.9;
        dynamic_title = true;
        key_bindings = [
          {
            key = "J";
            mods = "Alt";
            action = "ScrollLineDown";
          }
          {
            key = "K";
            mods = "Alt";
            action = "ScrollLineUp";
          }
          {
            key = "Y";
            mods = "Alt";
            action = "Copy";
          }
          {
            key = "P";
            mods = "Alt";
            action = "Paste";
          }
          {
            key = "Equals";
            mods = "Alt";
            action = "IncreaseFontSize";
          }
          {
            key = "Subtract";
            mods = "Alt";
            action = "DecreaseFontSize";
          }
        ];
      };
    };
    # Add and configure zsh
    zsh = {
      enable = true;
      autocd = true;
      dotDir = ".config/zsh";
      enableCompletion = true;
      enableAutosuggestions = true;
      defaultKeymap = "viins";
      shellAliases = aliases;
      initExtraBeforeCompInit = ''
        fpath=(~/.config/zsh/completion $fpath)

        source "$HOME/.guix-profile/etc/profile"
        source "$HOME/.config/guix/current/etc/profile"

        for file in $(find $HOME/.nix-profile/lib/common-lisp-settings/ -name '*.sh')
        do
          source $file
        done
      '';
      # Plugins
      plugins = [
        # sfz-prompt
        {
          name = "sfz";
          src = builtins.fetchGit {
            url = "https://github.com/teu5us/sfz-prompt.zsh";
          };
        }
        {
          name = "fzf-tab";
          src =
            builtins.fetchGit { url = "https://github.com/Aloxaf/fzf-tab"; };
        }
        {
          name = "zsh-autosuggestions";
          src = builtins.fetchGit {
            url = "https://github.com/zsh-users/zsh-autosuggestions";
          };
        }
        {
          name = "fast-syntax-highlighting";
          src = builtins.fetchGit {
            url = "https://github.com/desyncr/fast-syntax-highlighting";
          };
        }
        {
          name = "zsh-pandoc-completion";
          src = builtins.fetchGit { url = "https://github.com/srijanshetty/zsh-pandoc-completion"; };
        }
      ];
      initExtra = ''
        bindkey '^F' autosuggest-accept
        bindkey '^G' toggle-fzf-tab
        v () {
          #nvim $* && rm .nvimlog
          nvim $*
        }
        autoload -Uz v

        # indicate mode by cursor shape
        zle-keymap-select () {
        if [ $KEYMAP = vicmd ]; then
            printf "\033[2 q"
        else
            printf "\033[6 q"
        fi
        }
        zle-line-init () {
            zle -K viins
            printf "\033[6 q"
        }
        zle-line-finish () {
            printf "\033[2 q"
        }
        zle -N zle-keymap-select
        zle -N zle-line-init
        zle -N zle-line-finish
      '';
    };
    # Add and configure bash
    bash = {
      enable = true;
      enableAutojump = true;
      shellAliases = aliases;
      profileExtra = ''
        for file in $(find $HOME/.nix-profile/lib/common-lisp-settings/ -name '*.sh')
        do
          source $file
        done

        source "$HOME/.guix-profile/etc/profile"
        source "$HOME/.config/guix/current/etc/profile"
      '';
    };
    # Add and configure fzf
    fzf = {
      enable = true;
      defaultCommand = "fd --type f";
      defaultOptions = [ "--height 40%" "--prompt »" ];
      fileWidgetCommand = "fd --type f";
      fileWidgetOptions = [ "--preview 'head {}'" ];
      changeDirWidgetCommand = "fd --type d";
      changeDirWidgetOptions = [ "--preview 'tree -C {} | head -200'" ];
      historyWidgetOptions = [ "--tac" "--exact" ];
      enableBashIntegration = true;
      enableZshIntegration = true;
    };
    # Add and configure direnv
    direnv = {
      enable = true;
      enableBashIntegration = true;
      enableZshIntegration = true;
      enableNixDirenvIntegration = true;
    };
    # Add and configure Zathura
    zathura = {
      enable = true;
      options = {
        statusbar-h-padding = 0;
        statusbar-v-padding = 0;
        page-padding = 1;
      };
      # mappings
      extraConfig = ''
        map u scroll half-up
        map d scroll half-down
        map D toggle_page_mode
        map r reload
        map R rotate
        map K zoom in
        map J zoom out
        map i recolor
        map p print
      '';
    };
    # Add and configure mpv
    mpv = {
      enable = true;
      config = {
        vo = "gpu";
        hwdec = "auto-copy";
        hwdec-codecs = "all";
        sub-font-size = "22";
      };
      bindings = {
        "l" = "seek 5";
        "h" = "seek -5";
        "j" = "seek -60";
        "k" = "seek 60";
        "S" = "cycle sub";
      };
    };
  };

  services = {
    lorri.enable = true;
    dunst = {
      enable = true;
      settings = {
        global = {
          monitor = 0;
          follow = "keyboard";
          goemotry = "350x5-0+24";
          indicate_hidden = "yes";
          shrink = "yes";
          transparency = 20;
          notification_height = 0;
          separator_height = 2;
          padding = 0;
          horizontal_padding = 8;
          frame_width = 2;
          frame_color = "${nord4}";
          separator_color = "frame";
          sort = "yes";
          idle_threshold = 120;
          font = "Monospace 10";
          line_height = 0;
          markup = "full";
          format = ''
            <b>%s</b>
            %b'';
          alignment = "left";
          show_age_threshold = 60;
          word_wrap = "yes";
          ellipsize = "middle";
          ignore_newline = "no";
          stack_duplicates = true;
          hide_duplicate_count = true;
          show_indicators = "yes";
          icon_position = "left";
          max_icon_size = 40;
          sticky_history = "yes";
          history_length = 20;
          dmenu = "/usr/bin/dmenu -p dunst:";
          browser = "$BROWSER";
          always_run_script = true;
          title = "Dunst";
          class = "Dunst";
          startup_notification = false;
          force_xinerama = false;
        };
        experimantal = { per_monitor_dpi = false; };
        shortcuts = {
          close = "ctrl+space";
          close_all = "ctrl+shift+space";
          history = "ctrl+grave";
          context = "ctrl+shift+period";
        };
        urgency_low = {
          background = "${nord0}";
          foreground = "${nord4}";
          timeout = 5;
        };
        urgency_normal = {
          background = "${nord4}";
          foreground = "${nord0}";
          timeout = 5;
        };
        urgency_critical = {
          background = "#ff6c6b";
          foreground = "${nord4}";
          timeout = 5;
        };
      };
    };
    picom = {
      enable = true;
      blur = true;
      vSync = true;
    };
    unclutter = {
      enable = true;
      extraOptions = [ "ignore-scrolling" ];
    };
    udiskie = {
      enable = true;
      tray = "auto";
      automount = false;
      notify = true;
    };
  };

  fonts.fontconfig = { enable = true; };

  xsession = {
    enable = true;
    pointerCursor = {
      package = pkgs.numix-cursor-theme;
      name = "Numix-Cursor-Light";
      size = 28;
    };
    # windowManager.command = "${pkgs.dwm}/bin/dwm";
    windowManager.command = exwmCommand;
    # windowManager.command = "dbus-launch --sh-syntax --exit-with-session ${haskellPackages.xmonad}/bin/xmonad";
    initExtra = xinit;
  };

  xresources.properties = {
    # Emacs
    "Emacs.alpha" = 90;
    "Emacs.useXIM" = "true";
    # dwm
    "dwm.normbordercolor" = "${nord0}";
    "dwm.selbordercolor" = "${nord4}";
    "dwm.normbgcolor" = "${nord0}";
    "dwm.selbgcolor" = "${nord4}";
    "dwm.normfgcolor" = "${nord4}";
    "dwm.selfgcolor" = "${nord0}";
    # st
    "st.alpha" = "0.9";
    "st.font" = "Monospace-13";
    "st.termname" = "xterm-256color";
    "st.borderpx" = 2;
    ## Nord colors
    "*.foreground" = "${nord4}";
    "*.background" = "${nord0}";
    "*.cursorColor" = "${nord4}";
    "*fading" = 35;
    "*fadeColor" = "${nord3}";
    "*.color0" = "${nord1}";
    "*.color1" = "${nord11}";
    "*.color2" = "${nord14}";
    "*.color3" = "${nord13}";
    "*.color4" = "${nord9}";
    "*.color5" = "${nord15}";
    "*.color6" = "${nord8}";
    "*.color7" = "${nord5}";
    "*.color8" = "${nord3}";
    "*.color9" = "${nord11}";
    "*.color10" = "${nord14}";
    "*.color11" = "${nord13}";
    "*.color12" = "${nord9}";
    "*.color13" = "${nord15}";
    "*.color14" = "${nord7}";
    "*.color15" = "${nord6}";
  };

  gtk = {
    enable = true;
    theme = {
      package = pkgs.nordic;
      name = "Nordic";
    };
    iconTheme = {
      package = pkgs.paper-icon-theme;
      name = "Paper";
    };
    gtk2.extraConfig = ''
gtk-key-theme-name="Emacs"
'';
    gtk3.extraConfig = {
      gtk-key-theme-name = "Emacs";
    };
  };

  qt = {
    enable = false;
    platformTheme = "gtk";
  };

  nixpkgs.config.packageOverrides = pkgs: {
    nur = import (builtins.fetchTarball
      "https://github.com/nix-community/NUR/archive/master.tar.gz") {
        inherit pkgs;
      };
    st = pkgs.callPackage ./derive/st { patches = [ ./derive/st/my.patch ]; };
    dwm = pkgs.callPackage ./derive/dwm { };
    dmenu = pkgs.callPackage ./derive/dmenu { };
  };

  # home-env
  home = {
    packages = with pkgs; [
      gnome3.dconf
      bc
      xkb-switch
      aspell
      aspellDicts.ru
      aspellDicts.de
      aspellDicts.en
      libreoffice-fresh
      ranger
      feh
      sxiv
      xcalib
      mpv
      pulsemixer
      xcape
      st
      dwm
      dmenu
      clipnotify
      clipmenu
      maim
      ag
      fd
      ripgrep-all
      tmux
      tmate
      exa
      youtube-dl
      gimp-with-plugins
      unstable.python-language-server
    ];
    file = {
      # in $HOME
      ".xinitrc".text = ''
        $HOME/.xsession
      '';
      ".inputrc".text = ''
        set editing-mode vi
        set keymap vi-command
      '';
      ".haskeline".source = ./home/haskeline;
      ".ghci".source = ./home/ghci;
      ".guile".source = ./home/guile;
      ".racketrc".source = ./home/racketrc;
      ".less".source = ./home/less;
      ".agignore".source = ./home/agignore;
      ".xmonad/xmonad.hs".source = ./home/xmonad/xmonad.hs;
      ".xmobarrc".source = ./home/xmobarrc;
      ".tmate.conf".source = ./home/tmate.conf;
      ".tmux.conf".source = ./home/tmux.conf;
      # in $HOME/.config
      ".config/tmux" = {
        source = ./home/config/tmux;
        recursive = true;
      };
      ".config/zsh/completion/_docker".source =
        "${pkgs.docker}/share/zsh/site-functions/_docker";
      ".config/mimeapps.list".source = ./home/config/mimeapps.list;
      ".config/walls" = {
        source = ./home/config/walls;
        recursive = true;
      };
      ".config/emoji".source = ./home/config/emoji;
      ".config/fontconfig/fonts.conf".source =
        ./home/config/fontconfig/fonts.conf;
      ".config/Lenovo_Thinkpad_X220_IPS_1366x768.icm".source =
        ./home/config/Lenovo_Thinkpad_X220_IPS_1366x768.icm;
      ".config/t450s_mac.icc".source =
        ./home/config/t450s_mac.icc;
      ".config/ranger" = {
        source = ./home/config/ranger;
        recursive = true;
      };
      ".config/lf" = {
        source = ./home/config/lf;
        recursive = true;
      };
      ".config/nvim" = {
        source = ./home/config/nvim;
        recursive = true;
      };
      ".config/kmonad-keymap.kbd".source = if xsession.windowManager.command == exwmCommand
                                           then ./home/config/kmonad-keymap-emacs.kbd
                                           else ./home/config/kmonad-keymap-nonemacs.kbd;
      ".config/fish" = {
        source = ./home/config/fish;
        recursive = true;
      };
      # in $HOME/.local/bin
      ".local/bin" = {
        source = ./local/bin;
        recursive = true;
      };
    };
    # keyboard = {
    #   layout = "${kbdLayout}";
    #   variant = "${kbdVariant}";
    #   options = kbdOptions;
    # };
    sessionVariables = sessionVars;
  };

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "20.03";
}
