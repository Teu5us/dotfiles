with builtins;

let
  pkgs = import <nixpkgs> { };
  unstable = import <unstable> { };
in {
  allowUnfree = true;
  # Override default packages
  packageOverrides = pkgs:
    with pkgs; rec {
      # Custom NeoVim configuration
      myNeovim = neovim.override {
        configure = {
          customRC = builtins.readFile ./init.vim;
          packages.myNeovimPackage = with pkgs.vimPlugins; {
            start = [
              # comfort
              auto-pairs
              vim-repeat
              vim-unimpaired
              vim-surround
              vim-abolish
              fugitive
              gitgutter
              tcomment_vim
              fzf-vim
              vim-lion
              undotree
              tagbar
              # syntax
              vim-polyglot
              # appearance
              vim-startify
              lightline-vim
              papercolor-theme
              nord-vim
              molokai
              vim-indent-guides
              # coc main
              neco-vim
              coc-neco
              coc-nvim
              coc-pairs
              coc-lists
              coc-snippets
              coc-tabnine
              ultisnips
              vim-snippets
              # coc opt
              coc-html
              coc-css
              coc-python
              coc-emmet
              emmet-vim
              coc-json
              coc-rls
              # md
              vimwiki
              vim-table-mode
              goyo
              # linting
              ale
              # nix
              vim-nix
              vim-addon-nix
              # colors
              colorizer
              vCoolor-vim
              # haskell
              haskell-vim
              # direnv
              direnv-vim
            ];
            opt = [ ];
          };
        };
      };
      myHaskellEnv = pkgs.haskell.packages.ghc884.ghcWithHoogle
        (haskellPackages:
          with haskellPackages; [
            # haskell-language-server
            pandoc
            # tools
            cabal-install
            haskintex
            brittany
            # xmonad
            xmonad
            xmonad-contrib
            xmonad-extras
            xmonad-screenshot
            xmobar
            # emacs
            structured-haskell-mode
          ]);
      myEmacs = import ./derive/emacs/default.nix { };
      myREnv = rWrapper.override { packages = with rPackages; [ rmarkdown ]; };
      myPythonEnv = pkgs.python38.withPackages (python-packages:
        with python-packages; [
          # python-language-server
          pylint
          pyflakes
          black
          flake8
          autopep8
          yapf
          rope
          jedi
          setuptools

          ipython
          jupyter
          jupyterlab
          ipykernel
          ipyparallel

          numpy
          matplotlib
        ]);
      myChicken = pkgs.chicken.override { bootstrap-chicken = true; };
      myEggs = pkgs.callPackage ./derive/eggs/eggs.nix { };
      myHunspell = pkgs.hunspellWithDicts
        (with pkgs.hunspellDicts; [ en_GB-large en_US-large ru_RU ]);
      myVSCode = vscode-with-extensions.override {
        vscodeExtensions = (with vscode-extensions; [
          vscodevim.vim
          ms-python.python
          haskell.haskell
          justusadam.language-haskell
        ]) ++ vscode-utils.extensionsFromVscodeMarketplace [
          {
            name = "vscode-direnv";
            publisher = "Rubymaniac";
            version = "0.0.2";
            sha256 =
              "4d5be329d297784c699f2521f9f0cf97e79276543387b96df3149fc35620b4be";
          }
          {
            name = "vscode-django";
            publisher = "batisteo";
            version = "0.20.0";
            sha256 = "0cnh7754n2pz4z6m86if3zz8pfv3z1z7d9dnz4l64zp902np9dhk";
          }
          {
            name = "nix-env-selector";
            publisher = "arrterian";
            version = "0.1.2";
            sha256 = "1n5ilw1k29km9b0yzfd32m8gvwa2xhh6156d4dys6l8sbfpp2cv9";
          }
        ];
      };
      roswell = pkgs.callPackage ./derive/roswell { };
      c2ffi = pkgs.callPackage ./derive/c2ffi { };
      darknet = pkgs.callPackage ./derive/darknet { pkgs = pkgs; };
      new-sbcl = pkgs.sbcl.overrideAttrs (attrs: rec {
        pname = attrs.pname;
        version = "2.1.2";
        src = fetchurl {
          url = "mirror://sourceforge/project/${pname}/${pname}/${version}/${pname}-${version}-source.tar.bz2";
          sha256 = "02scrqyp2izsd8xjm2k5j5lhn4pdhd202jlcb54ysmcqjd80awdp";
        };
        enableFeatures = attrs.enableFeatures ++ [ "sb-core-compression" "sb-xref-for-internals" ];
        patchPhase = builtins.concatStringsSep "\n"
          ([ "echo '\"${version}.nixos\"' > version.lisp-expr" ]
           ++ lib.tail (lib.splitString "\n" attrs.patchPhase));
      });
    };
}
