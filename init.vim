" source config if exists {{{
let confdir='$HOME/.config/nvim/'

function! SourceIfExists(file)
  if filereadable(expand(a:file))
    exe 'source' a:file
  endif
endfunction

function! SourceConf(file)
  call SourceIfExists(g:confdir . a:file)
endfunction
" }}}

call SourceConf('confs/base-settings.vim')

call SourceConf('confs/completion.vim')

call SourceConf('confs/keymap-setup.vim')

" Plugins {{{
call SourceConf('confs/fzf.vim')

if has('nvim-0.5.0')
  call SourceConf('confs/coc.vim_5')
else
  call SourceConf('confs/coc.vim')
endif

call SourceConf('confs/plugins-general.vim')

call SourceConf('confs/additionalPlugins.vim')

call SourceConf('confs/additionalPluginSettings.vim')
" }}}

call SourceConf('confs/appearance+ligtline.vim')

call SourceConf('confs/remaps.vim')

call SourceConf('confs/term.vim')

call SourceConf('confs/ftsetup.vim')

call SourceConf('OP.vim')

" delete nvimlog upon exit {{{
function! NvimlogDel()
  if filereadable("./.nvimlog")
    call system("rm ./.nvimlog")
  endif
endfunction
autocmd VimLeavePre . call NvimlogDel()
" }}}
