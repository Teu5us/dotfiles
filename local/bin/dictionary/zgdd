#!/usr/bin/env bash

# # Main directory with dictionaries.
# # You can set the directory with the GDDIR environment variable.
dictdir="$3"
[ -z "$3" ] && \
	if [ -z "$GDDIR" ]; then
		dictdir="$HOME/.gddir/"
	else
		dictdir="$GDDIR"
	fi

readdict(){
	mapfile -t dicts < <(find -L "$d"/ -name '*.dz' -printf "%f\n" | sort)
	for (( n=1; n<${#dicts[@]}+1; n++ ));
	do
		7z x -so "${d}/${dicts[$n-1]}" "*.dsl" | search
	done
	# 7z x -so "${d}/*.dz" "*.dsl" | search
}

search(){
	# The search command. Outputs a .txt/.dsl dictionary from an arhive to stdout, then sed finds the pattern and definitions until the dividing empty line.

	# Cyrillic dot and asterisk do not work for some reason without this substitution.
	searchpattern=$(echo "$searchpattern" | sed 's/\././g; s/\*/*/g')

	# # This one was used with preconverted dictionaries.
	# [ "$(uname)" = "Linux" ] && \
	# unzip -c "${d}/*.zip" "*.txt" 2>/dev/null | sed -n \

	# 7z x -so "${d}/*.dz" "*.dsl" 2>/dev/null | iconv -f UTF-16 -t UTF-8 | sed 's/\\\r$//g' | dos2unix | dos2unix | sed -n \
	# 7z x -so "${d}/*.dz" "*.dsl" 2>/dev/null | sed 's/\\\r$//g' | dos2unix 2>/dev/null | dos2unix 2>/dev/null | sed -n \
	# "/^${searchpattern}$/,/^\w/{
	# sed 's/\\\r$//g' | dos2unix 2>/dev/null | dos2unix 2>/dev/null | sed -n \
	dos2unix 2>/dev/null | dos2unix 2>/dev/null | sed -n \
			"/^${searchpattern}$/,/^[0-9a-zA-Zа-яА-Я]/p" | sed '$d'
}

format(){
	sed "s/^${searchpattern}$/]==>> &/g; s/\[[^]]*\]//g; s/\]//g; s/{[^}]*}//g; s/}//g; s/U[KS]\s*.*.wav//g; s/\s*.*\.wav//g; /\s*Thesaurus/d; /.*Main\sentry/d; /.*Idiom/d; /^\s*$/d"
	# sed "s/^${searchpattern}$/]==>> &/g"
}

interactive(){
	# Interactive search.
	# Asks for a pattern to search.
	# If the pattern is empty, asks again.
	# If the pattern is ":d", returns to dictionary selection.
	# If the pattern is ":q", exits.

	while true; do
		echo "> Search:"
		read -r searchpattern
		[ "$searchpattern" = ":d" ] && init
		[ "$searchpattern" = "" ] && interactive
		[ "$searchpattern" = ":q" ] && exit 1
		clear
		search="$(readdict | format)"

		# Comment out "| less" to output to stdout.
		[ -n "$search" ] && echo "$search" | fold -s -w130 | less --mouse
	done
}

checkpattern(){
	# If a pattern was not provided as an argument, asks for a pattern to search.
	# If a pattern was provided as an argument, performs a search.

	if [ -z "$*" ]; then
		interactive
	else
		searchpattern="$*"
		search="$(readdict | format)"

		# Comment out "| less" to output to stdout.
		[ -n "$search" ] && echo "$search" | fold -s -w130 | less --mouse
	fi
}

init(){
	# Check if dictionary name is provided.
	# If false, lists the $dictdir and asks to choose a folder containing dictionary files, then goes to pattern check.
	# If true, uses the provided dictionary and goes to pattern check.

	if [ -z "$2" ]; then
		echo "> Choose a dictionary:"

		mapfile -t src < <(find -L "$dictdir"/ -type d -printf "%f\n" | sed '1d' | sort)
		for (( k=1,i=${#src[@]}; k<${#src[@]}+1; k++,i-- ));
		do
			z=$(( ${#src[@]}-k+1 ))
			echo "[$z] : ${src[$i-1]}"
			if [ $k = ${#src[@]} ]; then
				echo && echo "[:q] : Quit"
				read -r ind
				if [ "$ind" != ":q" ]; then
					[ -n "$ind" ] && [ "$ind" -eq "$ind" ] 2>/dev/null || init
					[ "$ind" -gt ${#src[@]} ] && init
					dict="${src[$ind-1]}"
					d="${dictdir}/${dict}/"
					checkpattern "$1"
					echo "You have chosen ${dict}."
				else
					exit 1
				fi
			fi
		done

	else
		dict="$2"
		d="${dictdir}/${dict}/"
		checkpattern "$1"
	fi
}

# Start the script.
init "$1" "$2"
