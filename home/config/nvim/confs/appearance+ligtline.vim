let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set t_Co=256
set t_ut=

if exists("$TMUX")
        set notermguicolors
else
        set termguicolors
endif

set bg=dark
set laststatus=2
set cursorline

colo onedark
hi! Normal ctermbg=NONE guibg=NONE

highlight CocHighlightText ctermfg=Blue guifg=#15aabf
let g:lightline = {
\ 'colorscheme' : 'onedark',
\ 'active' : {
\ 	'left' : [ [ 'mode', 'paste' ],
\		[ 'gitbranch', 'readonly', 'filename', 'modified', 'winnum' ] ],
\   'right': [ [ 'lineinfo' ],
\              [ 'keymap', 'percent' ],
\              [ 'fileformat', 'fileencoding', 'filetype' ] ]
\ },
\ 'component_function' : {
\	'gitbranch' : 'fugitive#head',
\ 'winnum' : 'WindowNumber',
\ 'keymap' : 'KM' },
\ 'separator': { 'left': "\ue0b0", 'right': "\ue0b2" },
\ 'subseparator': { 'left': "\ue0b1", 'right': "\ue0b3" }
\ } "
