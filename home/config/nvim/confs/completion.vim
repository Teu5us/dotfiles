set omnifunc=syntaxcomplete#Complete
set wildmode=longest,list,full
set completeopt+=menuone,noselect,noinsert
set completeopt+=preview
autocmd CompleteDone * pclose
