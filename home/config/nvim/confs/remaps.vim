" Netrw {{{
nnoremap <leader>tf :Vexplore<CR>
nnoremap <leader>еа :Vexplore<CR>
" }}}

" Start/End of line {{{
map <leader>H ^
map <leader>L $
map <leader>h g^
map <leader>l g$
" }}}

" Opening files {{{
nnoremap <leader>ff :FzfFiles<CR>
nnoremap <leader>fh :FzfHistory<CR>
" }}}

" New bufs/tabs {{{
nnoremap <leader>w2 :new<space><CR>
nnoremap <leader>w3 :vnew<space><CR>
nnoremap <leader>w5 :tabnew<space><CR>
" }}}

" Moving/switching buffers {{{
nnoremap <leader>mv :vert sb<space>
nnoremap <leader>ms :sb<space>
nnoremap <leader>mt <C-w>T
" }}}

" Saving/quitting {{{
nnoremap <leader>fs :w<CR>
nnoremap <leader>bd :bd<CR>
nnoremap <leader>fq :q<CR>
nnoremap <leader>qq :qa<CR>
" }}}

" vimrc {{{
nnoremap <leader>fed :e ~/.config/nvim/init.vim<CR>
nnoremap <leader>feR :source ~/.config/nvim/init.vim<CR>
" }}}

" Searching {{{
" nmap \ /
noremap <leader>ds :let @/=""<cr>
nnoremap <leader>sff mq:FzfBLines<CR>
nnoremap <leader>sfw mq:FzfLines<CR>
nnoremap gb 'q
nnoremap <leader>sr :reg<CR>
nnoremap <leader>xc :FzfHistory:<CR>
nnoremap <leader>xs :FzfHistory/<CR>
nnoremap <leader>sm :FzfMarks<CR>
nnoremap <leader>ac :FzfCommands<CR>
nnoremap <leader>bt :tabs<CR>:tabn<C-b>
nnoremap <leader>bb :FzfBuffers<CR>
nnoremap <leader>: :FzfCommands<CR>
" }}}

" Spell-check {{{
nnoremap <leader>cs :setlocal spell! spelllang=en_us,ru_ru<CR>
" }}}

" Shortcutting split navigation {{{
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
" }}}

" Navigating with guides {{{
inoremap <leader><Tab> <Esc><Esc>/<++><Enter>"_c4l
vnoremap <leader><Tab> <Esc>/<++><Enter>"_c4l
map <leader><Tab> <Esc>/<++><Enter>"_c4l

function! AltLeader()
	inoremap <leader>' <Esc>/<+-><Enter>"_c4l
	vnoremap <leader>' <Esc>/<+-><Enter>"_c4l
	map <leader>' <Esc>/<+-><Enter>"_c4l
endfunction
com! AltLeader call AltLeader()
" }}}

" Replace and add start/end {{{
nmap <leader>S :%s##g<Left><Left>
vmap <leader>S :s##gc<Left><Left><Left>
vmap <leader>I :norm I
vmap <leader>A :norm A
" }}}

" Compile document, be it groff/LaTeX/markdown/etc. {{{
map <leader>zcc :w! \| !compiler <c-r>%<CR>
map <leader>zcd :w! \| !compiler <c-r>% && convdoc <c-r>%<CR>
map <leader>ad :!docspreview <c-r>%<CR><CR>
" autocmd BufWritePost *.rmd :!compiler %
" }}}

" Open corresponding .pdf/.html or preview {{{
map <leader>ap :!opout <c-r>%<CR><CR>
" }}}
