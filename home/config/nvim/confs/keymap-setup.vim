" au FocusGained * call system("xkb-switch -s 'us(dvorak-alt-intl)'")
nnoremap <C-z> /<C-^><C-c>
vnoremap <C-z> /<C-^><C-c>
lnoremap <C-z> <C-^>
inoremap <C-z> <C-^>
cnoremap <C-z> <C-^>
onoremap <C-z> <C-^>
inoremap <C-\> /
function! SetKeyMap()
  if system("setxkbmap -query | awk '/^variant/ {print $2}'") == "altgr-intl,\n"
    set keymap=russian-jcukenwin
  elseif system("setxkbmap -query | awk '/^variant/ {print $2}'") == "dvorak,\n"
    set keymap=ru-dvorak
  else
    set keymap=russian-jcukenmac
  endif
endfunction
call SetKeyMap()
set iminsert=0
set imsearch=-1

" Show active keymap
fun! KM()
  if &iminsert
    return b:keymap_name
  else
    return 'us'
  endif
endfu
com! KM call KM()
