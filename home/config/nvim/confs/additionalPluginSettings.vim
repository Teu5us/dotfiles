" Firenvim {{{
if exists('g:started_by_firenvim')
  set laststatus=0 noruler noshowcmd
  nmap <M-w> <C-w>
  inoremap <M-p> <C-p>
  inoremap <M-n> <C-n>
  inoremap <M-t> <C-t>
  inoremap <M-d> <C-d>
  inoremap <M-i> <C-i>
  inoremap <M-h> <C-h>
  inoremap <M-w> <C-w>
  au BufEnter * set ft=vimwiki
endif

let g:firenvim_config = { 
    \ 'globalSettings': {
        \ 'alt': 'all',
        \ '<C-w>': 'noop',
        \ '<C-n>': 'noop',
        \ '<C-t>': 'noop',
    \  },
    \ 'localSettings': {
        \ '.*': {
            \ 'cmdline': 'neovim',
            \ 'priority': 0,
            \ 'selector': 'textarea',
            \ 'takeover': 'never',
        \ },
    \ }
\ }
" }}}

" golden_size {{{
lua << EOF
local function ignore_by_buftype(types)
  local buftype = vim.api.nvim_buf_get_option(0, 'buftype')
  for _, type in pairs(types) do
    if type == buftype then
      return 1
    end
  end
end

local golden_size = require("golden_size")
-- set the callbacks, preserve the defaults
golden_size.set_ignore_callbacks({
  { ignore_by_buftype, {'quickfix', 'nofile'} },
  { golden_size.ignore_float_windows }, -- default one, ignore float windows
  { golden_size.ignore_by_window_flag }, -- default one, ignore windows with w:ignore_gold_size=1
})
EOF
" }}}

" vim-sneak {{{
let g:sneak#label = 1
function! SetLabels()
  if &iminsert
    let g:sneak#target_labels = "яжнладчхЖНГОЗЛАДЩЬЧ,Х0"
  else
    let g:sneak#target_labels = ";sftunq/SFGHLTUNRMQZ?0"
  endif
endfunction
au User SneakEnter call SetLabels()
" }}}
