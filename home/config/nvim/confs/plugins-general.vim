" VCoolor {{{
let g:vcoolor_disable_mappings = 1
let g:vcoolor_map = '<leader>tc'
" }}}
" Goyo {{{
	noremap <leader>agg :Goyo \| set bg=dark \| set linebreak<CR>
" }}}
" haskell {{{
  let g:haskell_enable_quantification = 1   " to enable highlighting of `forall`
  let g:haskell_enable_recursivedo = 1      " to enable highlighting of `mdo` and `rec`
  let g:haskell_enable_arrowsyntax = 1      " to enable highlighting of `proc`
  let g:haskell_enable_pattern_synonyms = 1 " to enable highlighting of `pattern`
  let g:haskell_enable_typeroles = 1        " to enable highlighting of type roles
  let g:haskell_enable_static_pointers = 1  " to enable highlighting of `static`
  let g:haskell_backpack = 1                " to enable highlighting of backpack keywords }}}
" polyglot {{{
	let g:polyglot_disabled = ['vue', 'markdown'] " }}}
" ale {{{
  let g:ale_set_loclist = 0
  let g:ale_set_quickfix = 1
  let g:ale_open_list = 0
  let g:ale_set_balloons = 1
  let g:ale_linter_aliases = {'vue': ['vue', 'javascript']}
  let g:ale_linters = {'vue': ['eslint', 'vls']} " }}}
" table-mode {{{
	let g:table_mode_corner_corner='|'
	let g:table_mode_corner = '|'
	let g:table_mode_header_fillchar='=' " }}}
" fugitive {{{
	nnoremap <leader>ags :Gstatus<CR>
	nnoremap <leader>agw :Gw<CR>
	nnoremap <leader>agW :Gwq<CR>
	nnoremap <leader>agdv :Gvdiff<CR>
	nnoremap <leader>agds :Gsdiff<CR>
	nnoremap <leader>agc :Gcommit<CR>
	nnoremap <leader>agb :Gblame<CR>
  " }}}
" easy-align {{{
  xmap ga <Plug>(EasyAlign)
  nmap ga <Plug>(EasyAlign)
  " }}}
" tagbar {{{
	nnoremap <F8> :TagbarToggle<CR>
  " }}}
" undotree {{{
  nnoremap <leader>ut :UndotreeToggle<CR>
  " }}}
" papercolor {{{
	let g:PaperColor_Theme_Options = {
	  \   'theme': {
	  \     'default': {
	  \       'transparent_background': 1,
    \       'allow_italic': 1,
    \       'allow_bold': 1
	  \     }
	  \   }
	  \ } " }}}
" nord-vim {{{
  let g:nord_italic = 1
  let g:nord_italic_comments = 1
  let g:nord_underline = 1
" }}}
" indent-guides {{{
	let g:indent_guides_enable_on_vim_startup = 0 " }}}
" vimwiki {{{
  let g:vimwiki_table_mappings = 0
  let g:vimwiki_table_auto_fmt = 0
  " Ensure files are read as what I want:
	let g:vimwiki_ext2syntax = {'.Rmd': 'markdown', '.rmd': 'markdown','.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
	let g:vimwiki_list = [{'path': '~/vimwiki', 'syntax': 'markdown', 'ext': '.md'}]
	let g:markdown_syntax_conceal = 0
  let g:vimwiki_global_ext = 1
  let g:vimwiki_folding = 'custom'
  set foldtext=MyFoldText()
  fu! MyFoldText()
  	let line = getline(v:foldstart)

  	" markdown frontmatter -- just take the next line hoping it would be
  	" title: Your title
  	if line =~ '^----*$'
  		let line = getline(v:foldstart+1)
  	endif

  	let indent = max([indent(v:foldstart)-v:foldlevel, 1])
  	let lines = (v:foldend - v:foldstart + 1)
  	let strip_line = substitute(line, '^//\|=\+\|["#]\|/\*\|\*/\|{{{\d\=\|title:\s*', '', 'g')
  	let strip_line = substitute(strip_line, '^[[:space:]]*\|[[:space:]]*$', '', 'g')
  	let text = strpart(strip_line, 0, winwidth(0) - v:foldlevel - indent - 6 - strlen(lines))
  	if strlen(strip_line) > strlen(text)
  		let text = text.'…'
  	endif
  	return repeat('▧', v:foldlevel) . repeat(' ', indent) . text .' ('. lines .')'
  endfu
  " }}}
  " }}}
" auto-pairs {{{
let g:AutoPairs = {'(':')',
                  \ '[':']',
                  \ '{':'}',
                  \ "'":"'",
                  \ '"':'"',
                  \ "`":"`",
                  \ '```':'```',
                  \ '"""':'"""',
                  \ "'''":"'''",
                  \ "«":"»"}
" }}}
