call plug#begin('~/.config/nvim/plugged')

Plug 'dm1try/golden_size'
Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }
Plug 'justinmk/vim-sneak'

call plug#end()
