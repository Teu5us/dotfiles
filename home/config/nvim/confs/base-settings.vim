let mapleader =" "
let maplocalleader ="-"

set shell=/bin/sh
set guicursor=
let $VTE_VERSION="100"
nnoremap c "_c
set nocompatible
filetype plugin indent on
syntax on
set encoding=utf-8
set number relativenumber
set splitbelow splitright
set lbr

set lazyredraw
set synmaxcol=200

set go=a
set mouse=a
set nohlsearch
set clipboard=unnamedplus
set scrolloff=7
set noshowmode
set noswapfile
set colorcolumn=81

set lcs=eol:$,tab:>-\|,space:·,nbsp:°,trail:¤
set nolist

set dictionary+=/usr/share/dict/american-english
set dictionary+=/usr/share/dict/ru

set title titlelen=0 titlestring=%{KM()}%(\ %)\|%(\ %)%(\ %)%<%F%=%(\ %)
" set title titlelen=0 titlestring=%{KM()}%(\ %)\|%(\ %)%(\ %)%<%F%=%(\ %)\|%(\ %)%l/%L-%P
