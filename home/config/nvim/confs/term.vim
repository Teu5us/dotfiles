au BufEnter opener :vertical resize 40
tnoremap <leader>ao <C-\><C-n>:q<CR>
tnoremap <C-q> <C-\><C-n>
tnoremap <expr> <C-R> '<C-\><C-N>"'.nr2char(getchar()).'pi'
tnoremap <C-p> <CR><C-\><C-n>
if has('nvim')
    au TermOpen  * setlocal norelativenumber nonumber
    au TermClose * setlocal   relativenumber   number | q!
endif
