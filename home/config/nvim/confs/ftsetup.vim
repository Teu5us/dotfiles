" Mutt {{{
autocmd BufRead,BufNewFile /tmp/neomutt* let g:goyo_width=80
autocmd BufRead,BufNewFile /tmp/neomutt* :Goyo \| set bg=dark
" }}}

" bmdirs/bmfiles {{{
autocmd BufWritePost ~/.config/bmdirs,~/.config/bmfiles !shortcuts
" }}}

" Xresources/Xdefaults {{{
autocmd BufWritePost *Xresources,*Xdefaults !xrdb %
" }}}

" Calcurse {{{
autocmd BufWritePost *Xresources,*Xdefaults !xrdb %
" }}}

" groff {{{
autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff
" }}}

" LaTeX {{{
autocmd BufRead,BufNewFile *.tex set filetype=tex
au BufNewFile,BufRead *.tex set tabstop=2 | set softtabstop=2 | set shiftwidth=2 | set textwidth=79 | set expandtab | set autoindent | set fileformat=unix
au InsertLeave *.tex w
" Runs a script that cleans out tex build files whenever I close out of a .tex file.
autocmd VimLeave *.tex !texclear %
fu! TEX()
  " Word count:
  " autocmd FileType tex map <leader>W :w !detex \| wc -w<CR>
  " Code snippets
  inoremap <buffer> ,fr \begin{frame}<Enter>\frametitle{}<Enter><Enter><++><Enter><Enter>\end{frame}<Enter><Enter><++><Esc>6kf}i
  inoremap <buffer> ,fi \begin{fitch}<Enter><Enter>\end{fitch}<Enter><Enter><++><Esc>3kA
  inoremap <buffer> ,exe \begin{exe}<Enter>\ex<Space><Enter>\end{exe}<Enter><Enter><++><Esc>3kA
  inoremap <buffer> ,em \emph{}<++><Esc>T{i
  inoremap <buffer> ,bf \textbf{}<++><Esc>T{i
  vnoremap <buffer> , <ESC>`<i\{<ESC>`>2la}<ESC>?\\{<Enter>a
  inoremap <buffer> ,it \textit{}<++><Esc>T{i
  inoremap <buffer> ,ct \textcite{}<++><Esc>T{i
  inoremap <buffer> ,cp \parencite{}<++><Esc>T{i
  inoremap <buffer> ,glos {\gll<Space><++><Space>\\<Enter><++><Space>\\<Enter>\trans{``<++>''}}<Esc>2k2bcw
  inoremap <buffer> ,x \begin{xlist}<Enter>\ex<Space><Enter>\end{xlist}<Esc>kA<Space>
  inoremap <buffer> ,ol \begin{enumerate}<Enter><Enter>\end{enumerate}<Enter><Enter><++><Esc>3kA\item<Space>
  inoremap <buffer> ,ul \begin{itemize}<Enter><Enter>\end{itemize}<Enter><Enter><++><Esc>3kA\item<Space>
  inoremap <buffer> ,li <Enter>\item<Space>
  inoremap <buffer> ,ref \ref{}<Space><++><Esc>T{i
  inoremap <buffer> ,tab \begin{tabular}<Enter><++><Enter>\end{tabular}<Enter><Enter><++><Esc>4kA{}<Esc>i
  inoremap <buffer> ,ot \begin{tableau}<Enter>\inp{<++>}<Tab>\const{<++>}<Tab><++><Enter><++><Enter>\end{tableau}<Enter><Enter><++><Esc>5kA{}<Esc>i
  inoremap <buffer> ,can \cand{}<Tab><++><Esc>T{i
  inoremap <buffer> ,con \const{}<Tab><++><Esc>T{i
  inoremap <buffer> ,v \vio{}<Tab><++><Esc>T{i
  inoremap <buffer> ,a \href{}{<++>}<Space><++><Esc>2T{i
  inoremap <buffer> ,sc \textsc{}<Space><++><Esc>T{i
  inoremap <buffer> ,chap \chapter{}<Enter><Enter><++><Esc>2kf}i
  inoremap <buffer> ,sec \section{}<Enter><Enter><++><Esc>2kf}i
  inoremap <buffer> ,ssec \subsection{}<Enter><Enter><++><Esc>2kf}i
  inoremap <buffer> ,sssec \subsubsection{}<Enter><Enter><++><Esc>2kf}i
  inoremap <buffer> ,st <Esc>F{i*<Esc>f}i
  inoremap <buffer> ,beg \begin{DELRN}<Enter><++><Enter>\end{DELRN}<Enter><Enter><++><Esc>4k0fR:MultipleCursorsFind<Space>DELRN<Enter>c
  inoremap <buffer> ,up <Esc>/usepackage<Enter>o\usepackage{}<Esc>i
  nnoremap <buffer> ,up /usepackage<Enter>o\usepackage{}<Esc>i
  inoremap <buffer> ,tt \texttt{}<Space><++><Esc>T{i
  inoremap <buffer> ,bt {\blindtext}
  inoremap <buffer> ,nu $\varnothing$
  inoremap <buffer> ,col \begin{columns}[T]<Enter>\begin{column}{.5\textwidth}<Enter><Enter>\end{column}<Enter>\begin{column}{.5\textwidth}<Enter><++><Enter>\end{column}<Enter>\end{columns}<Esc>5kA
  inoremap <buffer> ,rn (\ref{})<++><Esc>F}i
endfu
au FileType tex call TEX()
" }}}

" html {{{
au BufNewFile,BufRead *.html set tabstop=2 | set softtabstop=2 | set shiftwidth=2 | set textwidth=150 | set expandtab | set autoindent | set fileformat=unix

fu! HTML()
  inoremap <buffer> ,b <b></b><Esc>FbT>i
  inoremap <buffer> ,it <em></em><Esc>FeT>i
  inoremap <buffer> ,1 <h1></h1><Esc>2kf<i
  inoremap <buffer> ,2 <h2></h2><Esc>2kf<i
  inoremap <buffer> ,3 <h3></h3><Esc>2kf<i
  inoremap <buffer> ,p <p></p><Esc>0f>a
  inoremap <buffer> ,a <a<Space>href=""><++></a><Esc>14hi
  inoremap <buffer> ,e <a<Space>target="_blank"<Space>href=""><++></a><Esc>14hi
  inoremap <buffer> ,ul <ul><Enter><li></li><Enter></ul><Enter><Enter><++><Esc>03kf<i
  inoremap <buffer> ,li <Esc>o<li></li><Esc>F>a
  inoremap <buffer> ,ol <ol><Enter><li></li><Enter></ol><Enter><Enter><++><Esc>03kf<i
  inoremap <buffer> ,im <img src="" alt="<++>"><++><esc>Fcf"a
  inoremap <buffer> ,td <td></td><++><Esc>Fdcit
  inoremap <buffer> ,tr <tr></tr><Enter><++><Esc>kf<i
  inoremap <buffer> ,th <th></th><++><Esc>Fhcit
  inoremap <buffer> ,tab <table><Enter></table><Esc>O
  inoremap <buffer> ,gr <font color="green"></font><Esc>F>a
  inoremap <buffer> ,rd <font color="red"></font><Esc>F>a
  inoremap <buffer> ,yl <font color="yellow"></font><Esc>F>a
  inoremap <buffer> ,dt <dt></dt><Enter><dd><++></dd><Enter><++><esc>2kcit
  inoremap <buffer> ,dl <dl><Enter><Enter></dl><enter><enter><++><esc>3kcc
  inoremap <buffer> &<space> &amp;<space>
  inoremap <buffer> á &aacute;
  inoremap <buffer> é &eacute;
  inoremap <buffer> í &iacute;
  inoremap <buffer> ó &oacute;
  inoremap <buffer> ú &uacute;
  inoremap <buffer> ä &auml;
  inoremap <buffer> ë &euml;
  inoremap <buffer> ï &iuml;
  inoremap <buffer> ö &ouml;
  inoremap <buffer> ü &uuml;
  inoremap <buffer> ã &atilde;
  inoremap <buffer> ẽ &etilde;
  inoremap <buffer> ĩ &itilde;
  inoremap <buffer> õ &otilde;
  inoremap <buffer> ũ &utilde;
  inoremap <buffer> ñ &ntilde;
  inoremap <buffer> à &agrave;
  inoremap <buffer> è &egrave;
  inoremap <buffer> ì &igrave;
  inoremap <buffer> ò &ograve;
  inoremap <buffer> ù &ugrave;
endfu
au FileType html call HTML()
" }}}

" bib {{{
autocmd FileType bib inoremap ,a @article{<Enter>author<Space>=<Space>{<++>},<Enter>year<Space>=<Space>{<++>},<Enter>title<Space>=<Space>{<++>},<Enter>journal<Space>=<Space>{<++>},<Enter>volume<Space>=<Space>{<++>},<Enter>pages<Space>=<Space>{<++>},<Enter>}<Enter><++><Esc>8kA,<Esc>i
autocmd FileType bib inoremap ,b @book{<Enter>author<Space>=<Space>{<++>},<Enter>year<Space>=<Space>{<++>},<Enter>title<Space>=<Space>{<++>},<Enter>publisher<Space>=<Space>{<++>},<Enter>}<Enter><++><Esc>6kA,<Esc>i
autocmd FileType bib inoremap ,c @incollection{<Enter>author<Space>=<Space>{<++>},<Enter>title<Space>=<Space>{<++>},<Enter>booktitle<Space>=<Space>{<++>},<Enter>editor<Space>=<Space>{<++>},<Enter>year<Space>=<Space>{<++>},<Enter>publisher<Space>=<Space>{<++>},<Enter>}<Enter><++><Esc>8kA,<Esc>i
" }}}

" markdown {{{
function! MD()
  map <buffer> <leader>W yiWi[<esc>Ea](<esc>pa)
  map <buffer> <leader>Ц yiWi[<esc>Ea](<esc>pa)
  inoremap <buffer> ,n ---<Enter><Enter>
  inoremap <buffer> ,b ****<++><Esc>F*hi
  inoremap <buffer> ,c **<++><Esc>F*i
  inoremap <buffer> ,s ~~~~<++><Esc>F~hi
  inoremap <buffer> ,e **<++><Esc>F*i
  inoremap <buffer> ,i ![](<++>)<++><Esc>F[a
  inoremap <buffer> ,a [](<++>)<++><Esc>F[a
  inoremap <buffer> ,1 #<Space><Enter><++><Esc>kA
  inoremap <buffer> ,2 ##<Space><Enter><++><Esc>kA
  inoremap <buffer> ,3 ###<Space><Enter><++><Esc>kA
  inoremap <buffer> ,l --------<Enter>
  inoremap <buffer> ,p <++>

  " inoremap ,h ====<Space><++><Esc>F=hi
  " inoremap ,r ```{r}<CR>```<CR><CR><esc>2kO
  " inoremap ,p ```{python}<CR>```<CR><CR><esc>2kO
  " inoremap ,c ```<cr>```<cr><cr><esc>2kO
  source ~/.config/nvim/OP.vim
  endfu
" }}}

" wiki {{{
autocmd BufEnter *.wiki set filetype=wiki
au FileType wiki set tabstop=2 | set softtabstop=2 | set shiftwidth=2 | set textwidth=120 | set expandtab | set autoindent | set fileformat=unix
function! Wiki()
  " INSERT MAPPINGS
  inoremap <buffer> ,1 =<Space>~<Space>=<Esc>F~cl
  inoremap <buffer> ,2 ==<Space>~<Space>==<Esc>F~cl
  inoremap <buffer> ,3 ===<Space>~<Space>===<Esc>F~cl
  inoremap <buffer> ,b *~*<Space><++><Esc>F~cl
  inoremap <buffer> ,i _~_<Space><++><Esc>F~cl
  inoremap <buffer> ,s ~~#~~<Space><++><Esc>F#cl
  inoremap <buffer> ,c `~`<Space><++><Esc>F~cl
  inoremap <buffer> ,d ~::<Space><++><Esc>F~cl

  " VISUAL MAPPINGS
  vnoremap <buffer> ,xb "zdi**<Left><C-o>"zP<Esc>
  vnoremap <buffer> ,xi "zdi__<Left><C-o>"zP<Esc>
  vnoremap <buffer> ,xc "zdi``<Left><C-o>"zP<Esc>
  vnoremap <buffer> ,xs "zdi~~~~<Left><Left><C-o>"zP<Esc>
  vnoremap <buffer> ,xp "zdi{{{<CR>}}}<Up><C-o>"zp<Esc>

  " NORMAL MAPPINGS
  nnoremap <buffer> ,xb "zdiwi**<Left><C-o>"zP<Esc>
  nnoremap <buffer> ,xi "zdiwi__<Left><C-o>"zP<Esc>
  nnoremap <buffer> ,xc "zdiwi``<Left><C-o>"zP<Esc>
  nnoremap <buffer> ,xs "zdiwi~~~~<Left><Left><C-o>"zP<Esc>
  nnoremap <buffer> ,hh i=<Space>~<Space>=<Esc>F~cl
  nnoremap <buffer> ,h2h i==<Space>~<Space>==<Esc>F~cl
  nnoremap <buffer> ,h3h i===<Space>~<Space>===<Esc>F~cl
  nnoremap <buffer> ,iu i*<Space>
  nnoremap <buffer> ,io i#<Space>
endfunction
autocmd Filetype wiki call Wiki()
" }}}

" haskell {{{
au BufEnter *.hs setlocal foldmethod=marker | set foldmarker={{{,}}}
au BufNewFile,BufRead *.hs set tabstop=2 | set softtabstop=2 | set shiftwidth=2 | set textwidth=8 | set expandtab | set autoindent | set fileformat=unix
" }}}

" xml {{{
autocmd FileType xml inoremap ,e <item><Enter><title><++></title><Enter><guid<space>isPermaLink="false"><++></guid><Enter><pubDate><Esc>:put<Space>=strftime('%a, %d %b %Y %H:%M:%S %z')<Enter>kJA</pubDate><Enter><link><++></link><Enter><description><![CDATA[<++>]]></description><Enter></item><Esc>?<title><enter>cit
autocmd FileType xml inoremap ,a <a href="<++>"><++></a><++><Esc>F"ci"
" }}}

" vim {{{
au BufNewFile,BufRead *.vim set tabstop=2 | set softtabstop=2 | set shiftwidth=2 | set textwidth=120 | set expandtab | set autoindent | set fileformat=unix | set foldmethod=marker | set foldmarker={{{,}}}
" }}}

" py {{{
au BufNewFile,BufRead *.py set tabstop=4 | set softtabstop=4 | set shiftwidth=4 | set textwidth=80 | set expandtab | set autoindent | set fileformat=unix
let python_highlight_all=1
au BufEnter *.py map <buffer> <leader>rr :!python <C-r>%<CR>
" }}}

" js {{{
au BufNewFile,BufRead *.js set tabstop=2 | set softtabstop=2 | set shiftwidth=2 | set textwidth=80 | set expandtab | set autoindent | set fileformat=unix
" }}}

" json {{{
au BufNewFile,BufRead *.json set tabstop=2 | set softtabstop=2 | set shiftwidth=2 | set textwidth=80 | set expandtab | set autoindent | set fileformat=unix
" }}}

" pug {{{
autocmd BufEnter *.pug set filetype=pug
au BufNewFile,BufRead *.pug set tabstop=2 | set softtabstop=2 | set shiftwidth=2 | set textwidth=120 | set expandtab | set autoindent | set fileformat=unix
" }}}

" css,scss,sass {{{
au BufNewFile,BufRead *.css,*.scss,*.sass set tabstop=2 | set softtabstop=2 | set shiftwidth=2 | set textwidth=79 | set expandtab | set autoindent | set fileformat=unix
" }}}

" vue {{{
autocmd BufEnter *.vue set filetype=vue
au BufNewFile,BufRead *.vue set tabstop=2 | set softtabstop=2 | set shiftwidth=2 | set textwidth=80 | set expandtab | set autoindent | set fileformat=unix
" }}}

" elm {{{
au BufNewFile,BufRead *.elm set tabstop=2 | set softtabstop=2 | set shiftwidth=2 | set textwidth=119 | set expandtab | set autoindent | set fileformat=unix
" }}}
