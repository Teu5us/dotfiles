{ pkgs, stdenv }:
rec {
  inherit (pkgs) eggDerivation fetchegg;

  address-info = eggDerivation {
    name = "address-info-1.0.5";

    src = fetchegg {
      name = "address-info";
      version = "1.0.5";
      sha256 = "1nv87ghfv8szmi2l0rybrgrds6fs5w6jxafqslnzw0mw5sfj6jyk";
    };

    buildInputs = [
      srfi-1
    ];
  };

  apropos = eggDerivation {
    name = "apropos-3.5.1";

    src = fetchegg {
      name = "apropos";
      version = "3.5.1";
      sha256 = "0cacpz6w3gb964853ic70qd1wf8ma3gpfpp4kk9pqnbkfg71rm1l";
    };

    buildInputs = [
      srfi-1
      srfi-13
      check-errors
      string-utils
      symbol-utils
    ];
  };

  args = eggDerivation {
    name = "args-1.6.0";

    src = fetchegg {
      name = "args";
      version = "1.6.0";
      sha256 = "1y9sznh4kxqxvhd8k44bjx0s7xspp52sx4bn8i8i0f8lwch6r2g4";
    };

    buildInputs = [
      srfi-1
      srfi-13
      srfi-37
    ];
  };

  base64 = eggDerivation {
    name = "base64-1.0";

    src = fetchegg {
      name = "base64";
      version = "1.0";
      sha256 = "01lid9wxf94nr7gmskamxlfngack1hyxig8rl9swwgnbmz9qgysi";
    };

    buildInputs = [
      srfi-13
    ];
  };

  breadline = eggDerivation {
    name = "breadline-0.8";

    src = fetchegg {
      name = "breadline";
      version = "0.8";
      sha256 = "0az9flipmmh0q99gaqwxiibb68lxgpwbm73k9qfakf14k38iz96c";
    };

    buildInputs = [
      apropos
      pkgs.readline
    ];
  };

  check-errors = eggDerivation {
    name = "check-errors-3.1.2";

    src = fetchegg {
      name = "check-errors";
      version = "3.1.2";
      sha256 = "04r0s1iw2i8xmmgjjgbpnvhsphv83a04hdf4nn7iv7g7f9rncbyi";
    };

    buildInputs = [
      
    ];
  };

  chicken-doc = eggDerivation {
    name = "chicken-doc-0.6.1";

    src = fetchegg {
      name = "chicken-doc";
      version = "0.6.1";
      sha256 = "0p50dfyznashc7sxmvc9gv9bmcnjrvlrg99fw0qj7g585xczbjgs";
    };

    buildInputs = [
      matchable
      fmt
      sxml-transforms
      srfi-1
      srfi-13
      srfi-69
    ];
  };

  defstruct = eggDerivation {
    name = "defstruct-2.0";

    src = fetchegg {
      name = "defstruct";
      version = "2.0";
      sha256 = "0q1v1gdwqlpmwcsa4jnqldfqky9k7kvb83qgkhdyqym52bm5aln8";
    };

    buildInputs = [
      srfi-1
    ];
  };

  fmt = eggDerivation {
    name = "fmt-0.8.11.2";

    src = fetchegg {
      name = "fmt";
      version = "0.8.11.2";
      sha256 = "0xvq2v3zqyl02gankmjyr6m50pwnj12byh7d3rrhmnr8xpin09gp";
    };

    buildInputs = [
      srfi-1
      srfi-13
      srfi-69
      utf8
    ];
  };

  http-client = eggDerivation {
    name = "http-client-1.2";

    src = fetchegg {
      name = "http-client";
      version = "1.2";
      sha256 = "04nv73af0999b4pfn2lyj57yv8ssa6ajgl56lzrdw1s9sg77r0qf";
    };

    buildInputs = [
      intarweb
      uri-common
      simple-md5
      sendfile
      srfi-1
      srfi-13
      srfi-18
      srfi-69
    ];
  };

  input-parse = eggDerivation {
    name = "input-parse-1.2";

    src = fetchegg {
      name = "input-parse";
      version = "1.2";
      sha256 = "140gqyxsbgni2f9280wx02hkjz6h9xqhjcz3p2l8p9l6nlcs9rym";
    };

    buildInputs = [
      srfi-13
    ];
  };

  intarweb = eggDerivation {
    name = "intarweb-2.0.1";

    src = fetchegg {
      name = "intarweb";
      version = "2.0.1";
      sha256 = "0md226jikqhj993cw17588ipmnz0v7l34zrvylamyrs6zbvj3scm";
    };

    buildInputs = [
      srfi-1
      srfi-13
      srfi-14
      defstruct
      uri-common
      base64
    ];
  };

  iset = eggDerivation {
    name = "iset-2.2";

    src = fetchegg {
      name = "iset";
      version = "2.2";
      sha256 = "0yfkcd07cw6xnnqfbbvjy81x0vc54k40vdjp2m7gwxx64is6m3rz";
    };

    buildInputs = [
      
    ];
  };

  matchable = eggDerivation {
    name = "matchable-1.1";

    src = fetchegg {
      name = "matchable";
      version = "1.1";
      sha256 = "084hm5dvbvgnpb32ispkp3hjili8z02hamln860r99jx68jx6j2v";
    };

    buildInputs = [
      
    ];
  };

  memory-mapped-files = eggDerivation {
    name = "memory-mapped-files-0.4";

    src = fetchegg {
      name = "memory-mapped-files";
      version = "0.4";
      sha256 = "0by3r18bj9fs0bs9w5czx84vssmr58br3x7pz1m3myb4mns3mpsc";
    };

    buildInputs = [
      
    ];
  };

  miscmacros = eggDerivation {
    name = "miscmacros-1.0";

    src = fetchegg {
      name = "miscmacros";
      version = "1.0";
      sha256 = "0n2ia4ib4f18hcbkm5byph07ncyx61pcpfp2qr5zijf8ykp8nbvr";
    };

    buildInputs = [
      
    ];
  };

  openssl = eggDerivation {
    name = "openssl-2.0.5";

    src = fetchegg {
      name = "openssl";
      version = "2.0.5";
      sha256 = "1z1k61j10z4ayf8cqclp89a0sqhhi3ymvnhb7b6c5p6cv48arysb";
    };

    buildInputs = [
      srfi-13
      srfi-18
      address-info
      pkgs.openssl
    ];
  };

  regex = eggDerivation {
    name = "regex-2.0";

    src = fetchegg {
      name = "regex";
      version = "2.0";
      sha256 = "0qgqrrdr95yqggw8xyvb693nhizwqvf1fp9cjx9p3z80c4ih8j4j";
    };

    buildInputs = [
      
    ];
  };

  sendfile = eggDerivation {
    name = "sendfile-1.8.3";

    src = fetchegg {
      name = "sendfile";
      version = "1.8.3";
      sha256 = "0acmydjxlrbq7bdspmrzv9q9l3gh4xxnbpi5g1d5mz1g2mjwgm63";
    };

    buildInputs = [
      memory-mapped-files
    ];
  };

  shell = eggDerivation {
    name = "shell-0.4";

    src = fetchegg {
      name = "shell";
      version = "0.4";
      sha256 = "1z7vijlxkrpbmc77ds5hf6cy4knq7myczkad6p5h0yjmz7742mjd";
    };

    buildInputs = [
      
    ];
  };

  simple-md5 = eggDerivation {
    name = "simple-md5-0.1.1";

    src = fetchegg {
      name = "simple-md5";
      version = "0.1.1";
      sha256 = "1sn8lijm4znmg80bk02wmvirdf4smxk4h2jsq5an5b6ycbdx8c4b";
    };

    buildInputs = [
      memory-mapped-files
      srfi-13
    ];
  };

  srfi-1 = eggDerivation {
    name = "srfi-1-0.5.1";

    src = fetchegg {
      name = "srfi-1";
      version = "0.5.1";
      sha256 = "15x0ajdkw5gb3vgs8flzh5g0pzl3wmcpf11iimlm67mw6fxc8p7j";
    };

    buildInputs = [
      
    ];
  };

  srfi-13 = eggDerivation {
    name = "srfi-13-0.3";

    src = fetchegg {
      name = "srfi-13";
      version = "0.3";
      sha256 = "0yaw9i6zhpxl1794pirh168clprjgmsb0xlr96drirjzsslgm3zp";
    };

    buildInputs = [
      srfi-14
    ];
  };

  srfi-14 = eggDerivation {
    name = "srfi-14-0.2.1";

    src = fetchegg {
      name = "srfi-14";
      version = "0.2.1";
      sha256 = "0gc33cx4xll9vsf7fm8jvn3gc0604kn3bbi6jfn6xscqp86kqb9p";
    };

    buildInputs = [
      
    ];
  };

  srfi-18 = eggDerivation {
    name = "srfi-18-0.1.5";

    src = fetchegg {
      name = "srfi-18";
      version = "0.1.5";
      sha256 = "19fiprjffmdfp309lml9wfqnrxxrk163pwhp9r8q5983rm99zzm7";
    };

    buildInputs = [
      
    ];
  };

  srfi-37 = eggDerivation {
    name = "srfi-37-1.4";

    src = fetchegg {
      name = "srfi-37";
      version = "1.4";
      sha256 = "17f593497n70gldkj6iab6ilgryiqar051v6azn1szhnm1lk7dwd";
    };

    buildInputs = [
      
    ];
  };

  srfi-69 = eggDerivation {
    name = "srfi-69-0.4.1";

    src = fetchegg {
      name = "srfi-69";
      version = "0.4.1";
      sha256 = "1l102kppncz27acsr4jyi46q0r7g2lb6gdbkd6p3h1xmvwcnk2hl";
    };

    buildInputs = [
      
    ];
  };

  ssax = eggDerivation {
    name = "ssax-5.1.0";

    src = fetchegg {
      name = "ssax";
      version = "5.1.0";
      sha256 = "09gx09q78xr4lfnpx01zhjn2jp7cs62cpvhsjmcw1jnanqcm3d44";
    };

    buildInputs = [
      input-parse
      srfi-1
      srfi-13
    ];
  };

  string-utils = eggDerivation {
    name = "string-utils-2.3.1";

    src = fetchegg {
      name = "string-utils";
      version = "2.3.1";
      sha256 = "1m96iqgnfm3zasqa9vj0510271m09k2d8hlcb55dn2ixz42k102y";
    };

    buildInputs = [
      srfi-1
      srfi-13
      srfi-69
      miscmacros
      utf8
      check-errors
    ];
  };

  sxml-modifications = eggDerivation {
    name = "sxml-modifications-0.3";

    src = fetchegg {
      name = "sxml-modifications";
      version = "0.3";
      sha256 = "1kj6y5bmgwf8jcdc2p7nn0a0j46vq5729r1328ykr982kva8shvb";
    };

    buildInputs = [
      srfi-1
      sxpath
    ];
  };

  sxml-serializer = eggDerivation {
    name = "sxml-serializer-0.5";

    src = fetchegg {
      name = "sxml-serializer";
      version = "0.5";
      sha256 = "1xjshbcac5xkl5mbkg7a350x8n36567i37dnrc0hbix2i2h19c8m";
    };

    buildInputs = [
      srfi-1
      srfi-13
    ];
  };

  sxml-transforms = eggDerivation {
    name = "sxml-transforms-1.4.3";

    src = fetchegg {
      name = "sxml-transforms";
      version = "1.4.3";
      sha256 = "11hn1s2xf4nviskzp4pwqvfcx0nc6a3p8npvpvkfl3fv3i0l8cz7";
    };

    buildInputs = [
      srfi-13
    ];
  };

  sxpath = eggDerivation {
    name = "sxpath-1.0";

    src = fetchegg {
      name = "sxpath";
      version = "1.0";
      sha256 = "08v73g5z6zwki6c6wvp6i9hcwldrpdn7vpvzh8p4q53dvwwn1cqy";
    };

    buildInputs = [
      srfi-13
    ];
  };

  symbol-utils = eggDerivation {
    name = "symbol-utils-2.1.0";

    src = fetchegg {
      name = "symbol-utils";
      version = "2.1.0";
      sha256 = "17nq8bj18f3bbf3mdfx1m8agy97izn1xcl8ymvgvvd5g384b2xil";
    };

    buildInputs = [
      check-errors
    ];
  };

  uri-common = eggDerivation {
    name = "uri-common-2.0";

    src = fetchegg {
      name = "uri-common";
      version = "2.0";
      sha256 = "07rq7ppkyk3i85vqspc048pnj6gmjhj236z00chslli9xybqkgrd";
    };

    buildInputs = [
      uri-generic
      defstruct
      matchable
      srfi-1
      srfi-13
      srfi-14
    ];
  };

  uri-generic = eggDerivation {
    name = "uri-generic-3.2";

    src = fetchegg {
      name = "uri-generic";
      version = "3.2";
      sha256 = "1lpvnk1mnhmrga149km7ygpy7fkq7z2pvw0mvpx2aql03l8gpdsj";
    };

    buildInputs = [
      matchable
      srfi-1
      srfi-14
    ];
  };

  utf8 = eggDerivation {
    name = "utf8-3.6.2";

    src = fetchegg {
      name = "utf8";
      version = "3.6.2";
      sha256 = "10wzp3qmwik4gx3hhaqm2n83wza0rllgy57363h5ccv8fga5nnm6";
    };

    buildInputs = [
      srfi-69
      iset
      regex
    ];
  };
}

