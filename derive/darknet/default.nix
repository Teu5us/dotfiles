{ pkgs ? import <nixpkgs> { }
, stdenv ? pkgs.stdenv
, config ? pkgs.config
, enableCuda ? (config.cudaSupport or false) && stdenv.hostPlatform.isx86_64
, enableCudnn ? (config.cudnnSupport or false) && stdenv.hostPlatform.isx86_64
, enableOpencv ? true
, enableOpenmp ? false
, ... }:

let
  CUDA = if enableCuda then "1" else "0";
  cuDNN = if enableCudnn then "1" else "0";
  openCV = if enableOpencv then "1" else "0";
  openMP = if enableOpenmp then "1" else "0";
  openCV-package = (pkgs.opencv3.override { enableCuda = enableCuda; });
in
stdenv.mkDerivation rec {
  name = "darknet";
  version = "master-2021.02.24";
  src = pkgs.fetchgit {
    url = "https://github.com/pjreddie/darknet.git";
    rev = "a3714d0a2bf92c3dea84c4bea65b2b0c64dbc6b1";
    sha256 = "08mccmkdkzll155q0ji351d6716gdy01qh365vazsmwqs1n0z6yd";
  };
  nativeBuildInputs = with pkgs; [
    stdenv.cc
    pkg-config
    gnumake
    gnused
  ];
  buildInputs = with pkgs;
    []
    ++ lib.optional enableOpencv openCV-package
    ++ lib.optionals enableCuda [ cudatoolkit nvidia-optical-flow-sdk ]
    ++ lib.optional enableCudnn cudnn;
  buildPhase = with pkgs; ''
    # sed -i '45,46s/opencv/opencv4/' Makefile
    PKG_CONFIG_PATH=${lib.makeSearchPath "lib/pkgconfig" [ openCV-package ]}
    make GPU=${CUDA} CUDNN=${cuDNN} OPENCV=${openCV}
  '';
  installPhase = ''
    mkdir -p $out/bin $out/lib
    cp darknet $out/bin/
    cp libdarknet.so $out/lib/
    cp -r include $out/
  '';
}
