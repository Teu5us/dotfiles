# { lib, pkgs, ... }:
{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let
  lp = llvmPackages_10;
in
stdenv.mkDerivation rec {
  name = "c2ffi";
  version = "10.0.0";
  src = builtins.fetchGit {
    url = "https://github.com/rpav/c2ffi.git";
    ref = "llvm-10.0.0";
    # sha256 = "0ak72gc8gdlzwgh01k86h3izdvvkna85vygf3rr03g9vxjzwfn0l";
  };
  nativeBuildInputs = [ cmake ];
  buildInputs = [
    stdenv.cc
    lp.clang
    lp.llvm
    libffi
    lp.lld
  ];
  CXXFLAGS = "-I${lp.clang.cc}/include -L${lp.libclang}/lib -L${lp.clang}/lib";
  # LDFLAGS = "-lc++abi";
  cmakeFlags = [
    "-DBUILD_CONFIG=Release"
    "-DCMAKE_INSTALL_BINDIR=$out/bin"
  ];
  # unpackPhase = '''';
  # buildPhase = ''
  #   mkdir $src/build
  #   cd $src/build
  #   cmake -DBUILD_CONFIG=Release ..
  #   make
  # '';
  # installPhase = ''
  #   cp -r bin $out/
  # '';
}
