{ stdenv, fetchFromGitHub, pkgconfig, libX11, libXtst, openal, alure }:

stdenv.mkDerivation rec {
  name = "bucklespring";
  src = fetchFromGitHub {
    owner = "zevv";
    repo = "bucklespring";
    rev = "a88992206edede53ab280a98651caebe10b66f59";
    sha256 = "14g26cv2fharbgcacw59ybxrw9vzxrwpq1vxpj5x42i79hixs2iq";
  };
  buildInputs = [
    pkgconfig
    libX11
    libXtst
    openal
    alure
  ];
  buildPhase = ''
    make PATH_AUDIO="$out"/usr/share/bucklespring
  '';
  installPhase = ''
    mkdir -p $out/bin
    mkdir -p $out/usr/share/bucklespring
    install -m755 -D buckle "$out"/bin/buckle
    install -m644 -D -t "$out"/usr/share/bucklespring wav/*
  '';
}
