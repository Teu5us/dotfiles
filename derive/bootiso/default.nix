{ stdenv
, pkgs
, lib
, fetchFromGitHub
, utillinux
, e2fsprogs
, dosfstools
, ntfs3g
, wimlib
, syslinux
, glibc
, ncurses
, coreutils
, binutils-unwrapped
, findutils
, jq
, sedutil
, gnugrep
, file
, gawk
, rsync
, curl
, gnutar
, bc
, makeWrapper
}:

stdenv.mkDerivation rec {
  name = "bootiso-latest";

  src = fetchFromGitHub {
    owner = "jsamr";
    repo = "bootiso";
    rev = "latest";
    sha256 = "1faik189mbf4x9ngf232yjdyzazlp5r6j7va5s5ziq550q8k5z66";
  };

  deps = [
    utillinux
    e2fsprogs
    dosfstools
    ntfs3g
    wimlib
    syslinux
    glibc
    ncurses
    coreutils
    binutils-unwrapped
    findutils
    jq
    sedutil
    gnugrep
    file
    gawk
    rsync
    curl
    gnutar
    bc
  ];

  buildInputs = [ makeWrapper ];

  patchPhase = ''
sed -i "3s#/usr/local#$out#" Makefile
sed -i "52s#/usr/lib#${pkgs.syslinux}/share#;1323d;1325,1328d" bootiso
'';

  postInstall = ''
makeWrapper $out/bin/bootiso $out/bin/Bootiso --prefix PATH : ${lib.makeBinPath deps} # --prefix BOOTISO_SYSLINUX_LIB_ROOT : "${pkgs.syslinux}/share/syslinux"
'';

  meta = with stdenv.lib; {
      description = "Burn ISO to flash drive.";
      homepage = https://github.com/jsamr/bootiso;
      license = licenses.gpl3;
      # maintainers = with maintainers; [ paul ];
      platforms = platforms.linux;
  };
}
