/* This is a nix expression to build Emacs and some Emacs packages I like
   from source on any distribution where Nix is installed. This will install
   all the dependencies from the nixpkgs repository and build the binary files
   without interfering with the host distribution.

   To build the project, type the following from the current directory:

   $ nix-build emacs.nix

   To run the newly compiled executable:

   $ ./result/bin/emacs
*/
{ pkgs ? import <nixpkgs> { } }:

let
  myEmacs = (pkgs.emacs.override {
    withGTK3 = true;
    withGTK2 = false;
    # imagemagick = pkgs.imagemagickBig;
  }).overrideAttrs (attrs: {
    postInstall = (attrs.postInstall or "") + ''
      rm $out/share/applications/emacs.desktop
    '';
  });
  emacsWithPackages = (pkgs.emacsPackagesGen myEmacs).emacsWithPackages;
in emacsWithPackages (epkgs:
  (with epkgs.melpaPackages; [
    writeroom-mode
    pdf-tools
    vterm
    hamlet-mode
    gruber-darker-theme
    highlight-symbol
    scheme-complete
    lispy
    evil-lispy
  ]))
