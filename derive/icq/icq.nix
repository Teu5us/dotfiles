{ pkgs ? import <nixpkgs> {} }:
with pkgs;

let
  inherit stdenv libX11 libXrandr libXinerama libXcursor libXext libXdamage libXfixes libXcomposite libxcb xkeyboard_config fontconfig libxkbcommon libxkbfile;
in
{
  icq = stdenv.mkDerivation rec {
    name = "icq";
    src = ./icq.tar.xz;
    setSourceRoot = "sourceRoot=`pwd`";
    nativeBuildInputs = [ autoPatchelfHook ];
    buildInputs = [ xorg.libX11 xorg.libXrandr xorg.libXinerama xorg.libXcursor xorg.libXext xorg.libXdamage xorg.libXfixes xorg.libXcomposite xorg.libxcb xkeyboard_config fontconfig libxkbcommon xorg.libxkbfile ];
    # libPath = stdenv.lib.makeLibraryPath [ xorg.libX11 xorg.libXrandr xorg.libXinerama xorg.libXcursor xorg.libXext xorg.libXdamage xorg.libXfixes xorg.libXcomposite xorg.libxcb xkeyboard_config fontconfig libxkbcommon xorg.libxkbfile ];
    unpackPhase =
      ''
        tar xvf ${src}
      '';
    configurePhase = '''';
    patchPhase = '''';
    buildPhase = '''';
    installPhase =
      ''
        mkdir -p "$out"/bin
        # patchelf --interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" \
        #                --set-rpath $libPath:\$ORIGIN/../Libraries \
        #                $out/${name}
        #       makeWrapper $out/${name} \
        #           $out/bin/${name} \
        #           --prefix "QT_XKB_CONFIG_ROOT" ":" "${xkeyboard_config}/share/X11/xkb"
        install -m755 -D  ${name} "$out"/bin/${name}
        echo -e '#!/bin/sh\nexport QT_XKB_CONFIG_ROOT=${xkeyboard_config}/share/X11/xkb\n${icq}/bin/icq' > "$out"/bin/Icq
        chmod 755 "$out"/bin/Icq
      '';
  };
}
