{ stdenv
, libX11
, libXrandr
, libXinerama
, libXcursor
, libXext
, libXdamage
, libXfixes
, libXcomposite
, libxcb
, xkeyboard_config
, fontconfig
, libxkbcommon
, libxkbfile
, autoPatchelfHook }:

stdenv.mkDerivation rec {
    name = "icq";
    src = ./icq.tar.xz;
    setSourceRoot = "sourceRoot=`pwd`";
    nativeBuildInputs = [ autoPatchelfHook ];
    buildInputs =
      [ libX11
        libXrandr
        libXinerama
        libXcursor
        libXext
        libXdamage
        libXfixes
        libXcomposite
        libxcb
        xkeyboard_config
        fontconfig
        libxkbcommon
        libxkbfile
      ];
    unpackPhase =
      ''
        tar xvf ${src}
      '';
    configurePhase = '''';
    patchPhase = '''';
    buildPhase = '''';
    installPhase =
      ''
        mkdir -p "$out"/bin
        install -m755 -D  ${name} "$out"/bin/${name}
        # echo -e '#!/bin/sh\nexport QT_XKB_CONFIG_ROOT=${xkeyboard_config}/share/X11/xkb\n/bin/icq' > "$out"/bin/Icq
        cat <<- EOF > "$out"/bin/ICQ
        #!/bin/sh
        export QT_XKB_CONFIG_ROOT=${xkeyboard_config}/share/X11/xkb
        "$out"/bin/icq
        EOF
        chmod 755 "$out"/bin/ICQ
      '';
}
